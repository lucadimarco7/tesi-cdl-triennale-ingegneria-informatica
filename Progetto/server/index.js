const express=require('express'); //Per gestire il routing (req,res)
const mysql=require('mysql'); //Database MySQL
const cors=require('cors'); //Cross-Origin Resource Sharing, per indicare al browser che ha l'autorizzazione per accedere alle risorse
const bcrypt=require('bcrypt'); //Per criptare le password nel database
const session=require('express-session'); //Per gestire le sessioni
const cookieParser=require('cookie-parser'); //Per gestire i cookie
const fileUpload=require('express-fileupload'); //Per gestire l'upload dei file da client a server
const fs=require('fs'); //FileSystem
var multer=require('multer'); //Gestione dei file caricati

//Costanti e variabili dei moduli inclusi ('require')
const app=express();
var upload = multer({dest:'uploads/',limits:{fileSize:'50mb'}});
var expiryDate = new Date( Date.now() + 60 * 60 * 1000 );

//app.use per tutti i middleware da usare nell'app
app.use(fileUpload());
app.use(express.json({parameterLimit: 100000,limit: '50mb'})); //Analizza le richieste in arrivo con payload JSON
app.use(cors({origin:["http://localhost:3000"],methods:["GET","POST"],credentials: true}));
app.use(cookieParser());
app.use(session({name:'sessionId',key:'userid',resave:false,saveUninitialized:false,secret:'sdlfjljrowuroweu',cookie:{httpOnly: true,expires:expiryDate}}))

const saltRounds=10; //Testo random per criptare le password

//Configurazione database
const db=mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"agora"
})

//Recupera le materie dal database
app.get('/recuperamaterie',(req,res)=>{
    db.query("SELECT * FROM materia",(err,result)=>{
        res.send(result)
    })  
})

//Registrazione utente
app.post('/registra',(req,res)=>{
    var immagine=null; //Inizializza l'immagine del profilo
    //Imposta un'immagine di profilo di default
    fs.readFile('../client/src/images/defaultprofilepicture.jpg',function(err,data){immagine=data;});
    //Crea le costanti con i dati del form ricevuti con la richiesta post
    const nome=req.body.nome;
    const cognome=req.body.cognome;
    const nomeutente=req.body.nomeutente;
    const email=req.body.email;
    const password=req.body.password;
    const professione=req.body.professione;
    const matricola=req.body.matricola;
    const materie=req.body.materie;
    
    let i; //Per scorrere il vettore delle materie
    
    //Verifica correttezza dei campi del form
    db.query("SELECT * FROM utente WHERE nomeutente=?",nomeutente,(err,result)=>{
        if(result[0]!=null){
            res.send({msg:"Nome utente non disponibile",reg:false});
        }else{
            db.query("SELECT * FROM utente WHERE email=?",email,(err,result)=>{
                if(result[0]!=null){
                    res.send({msg:"E-mail già presente",reg:false})
                }else{
                    db.query("SELECT * FROM studente WHERE matricola=?",matricola,(err,result)=>{
                        if(result[0]!=null && professione=='1'){
                            res.send({msg:"Matricola già presente",reg:false});
                        }else{
                            //Se tutti i campi sono corretti crea l'utente nel database
                            bcrypt.hash(password,saltRounds,(err,hash)=>{
                                db.query("INSERT INTO utente (nome,cognome,nomeutente,email,password,immagine) VALUES (?,?,?,?,?,?)",[nome,cognome,nomeutente,email,hash,immagine],(err,result)=>{
                                        db.query("SELECT id FROM utente WHERE nomeutente=?",nomeutente,(err,result)=>{
                                            if(professione=='1'){//crea uno studente
                                                db.query("INSERT INTO studente (id,matricola) VALUES (?,?)",[result[0].id,matricola]);
                                                res.send({msg:"Registrazione avvenuta con successo",reg:true});
                                            }else if(professione=='2'){//crea un insegnante
                                                db.query("INSERT INTO insegnante (id) VALUES (?)",result[0].id);
                                                for(i=0;i<materie.length;i++){
                                                    db.query("INSERT INTO insegnamento (insid,matkey) VALUES (?,?)",[result[0].id,materie[i]]);
                                                }
                                                res.send({msg:"Registrazione avvenuta con successo",reg:true});
                                            }
                                        })
                                    })
                                }) 
                            }
                    })
                }
            })
        }
    })
})

//Accesso utente
app.post('/login',(req,res)=>{
    //Crea le costanti con i dati del form ricevuti con la richiesta post
    const nomeutente=req.body.nomeutente;
    const password=req.body.password;
    
    db.query("SELECT * FROM utente WHERE nomeutente=?",nomeutente,
    (err,result)=>{
        if(result.length>0){
            bcrypt.compare(password,result[0].password,(error,response)=>{ //Se esiste l'utente nel database compara la password inserita con quella del database
                if(response){
                    req.session.user=result;
                    res.send({msg:"Accesso eseguito",loggedIn:true});
                }else{
                    res.send({msg:"Combinazione nome utente/password errata",loggedIn:false});
                }
            });
        }else{
            res.send({msg:"Utente inesistente",loggedIn:false}); //Se l'utente non esiste nel database ritorna questo messaggio
        }
    })
})

//Verifica se un utente ha effettuato l'accesso
app.get('/login',(req,res)=>{
    if(req.session.user){ //Se esiste una sessione ritorna un oggetto con uno stato 'true' e il nome dell'utente
        res.send({loggedIn:true,user:req.session.user})
    }else{ //Se non esiste nessuna sessione ritorna un oggetto con uno stato 'false'
        res.send({loggedIn:false}) 
    }
})

//Recupera le informazioni legate all'utente dal database
app.get('/recuperaprofilo',(req,res)=>{
    db.query("SELECT * FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT studente.id, studente.matricola from studente INNER JOIN utente ON studente.id=?",result[0].id,(err,result2)=>{
            if(result2[0]!=null){
                res.send({isStudente:true,immagine:result[0].immagine,nome:result[0].nome,cognome:result[0].cognome,email:result[0].email,matricola:result2[0].matricola,descrizione:result[0].descrizione});
            }else{
                db.query("SELECT materia.label, materia.key, insegnamento.insid FROM materia INNER JOIN insegnamento ON materia.key=insegnamento.matkey WHERE insid=?",result[0].id,(err,result3)=>{
                    res.send({isStudente:false,immagine:result[0].immagine,nome:result[0].nome,cognome:result[0].cognome,email:result[0].email,materie:result3,descrizione:result[0].descrizione});    
                })
            }
        })  
    })  
})

//Recupera le informazioni legate all'utente richiesto dal database
app.post('/recuperaprofilo',(req,res)=>{
    const id=req.body.id
    let segui=false;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(req,result4)=>{
        db.query("SELECT * FROM segue WHERE id1=? AND id2=?",[result4[0].id,id],(err,result5)=>{
            if(result5[0]!=null){segui=true}
            db.query("SELECT * FROM utente WHERE id=?",id,(err,result)=>{
                db.query("SELECT studente.id, studente.matricola from studente INNER JOIN utente ON studente.id=?",result[0].id,(err,result2)=>{
                    if(result2[0]!=null){
                        res.send({isStudente:true,id:id,segui:segui,immagine:result[0].immagine,nome:result[0].nome,nomeutente:result[0].nomeutente,cognome:result[0].cognome,email:result[0].email,matricola:result2[0].matricola,descrizione:result[0].descrizione});
                    }else{
                        db.query("SELECT materia.label, insegnamento.insid FROM materia INNER JOIN insegnamento ON materia.key=insegnamento.matkey WHERE insid=?",result[0].id,(err,result3)=>{
                            res.send({isStudente:false,id:id,segui:segui,immagine:result[0].immagine,nome:result[0].nome,nomeutente:result[0].nomeutente,cognome:result[0].cognome,email:result[0].email,materie:result3,descrizione:result[0].descrizione});        
                        })
                    }
                })
            })
        })
    }) 
})

//Recupera la lista degli utenti dal database
app.get('/recuperautenti',(req,res)=>{
    db.query("SELECT * FROM utente WHERE nomeutente <> ?",req.session.user[0].nomeutente,(err,result)=>{
        res.send(result);
    })
})

//Inserisce nella tabella segui l'id dei due utenti
app.post('/segui',(req,res)=>{
    const id=req.body.id;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("INSERT INTO segue (id1,id2) VALUE (?,?)",[result[0].id,id],(err,result)=>{
            res.send(result);
        })
    })
})

//Rimuove dalla tabella segui l'id dei due utenti
app.post('/rimuovisegui',(req,res)=>{
    const id=req.body.id;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("DELETE FROM segue WHERE id1=? AND id2=?",[result[0].id,id],(err,result)=>{
            res.send(result);
        })
    })
})

//Recupera la lista degli utenti seguiti
app.get('/utentiseguiti',(req,res)=>{
    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT utente.immagine, utente.nome, utente.cognome FROM utente INNER JOIN segue on id2=utente.id WHERE id1=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Recupera gli avvisi dal database
app.get('/recuperaavvisi',(req,res)=>{
    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT avviso.titolo,avviso.corpo,utente.nomeutente,avviso.data FROM avviso INNER JOIN utente ON avviso.id=utente.id INNER JOIN segue on avviso.id=segue.id2 WHERE segue.id1=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Recupera i files dal database
app.get('/files',(req,res)=>{
    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT documento.id,documento.iddocumento,documento.titolo,documento.contenuto,utente.nomeutente FROM documento INNER JOIN utente ON documento.id=utente.id INNER JOIN segue on documento.id=segue.id2 WHERE segue.id1=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Prende il file presente sul database corrispondente all'indice richiesto
app.get('/download',(req,res)=>{
    const index=req.query.index;

    db.query("SELECT * FROM documento WHERE iddocumento=?",index,(err,result)=>{
        res.send(result[0].contenuto);
    })
})

//Recupera i test dal database
app.get('/recuperatest',(req,res)=>{
    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT test.id,test.idtest,test.titolo,utente.nomeutente FROM test INNER JOIN utente ON test.id=utente.id INNER JOIN segue on test.id=segue.id2 WHERE segue.id1=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Recupera le domande corrispondenti all'id del test dal database
app.post('/recuperadomande',(req,res)=>{
    const idtest=req.body.id;

    db.query("SELECT * FROM domanda WHERE idtest=?", idtest,(err,result)=>{
        res.send(result);
    })
})

//Recupera gli appelli dal database
app.get('/libretto',(req,res)=>{
    let i=0;
    let numero=0;
    let voti=0;
    let votip=0;
    let crediti=0;
    let media=0;
    let mediap=0;
    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT * FROM appello WHERE id=?",result[0].id,(err,result2)=>{
            for(i=0;i<result2.length;i++){
                if(result2[i].voto===0){
                }else{
                    numero++;
                    voti=voti+result2[i].voto;
                    votip=votip+(result2[i].voto*result2[i].crediti);
                    crediti=crediti+result2[i].crediti;
                }
            }
            media=voti/numero;
            mediap=votip/crediti;
            db.query("SELECT SUM(crediti) as crediti FROM appello WHERE id=?",result[0].id,(err,result3)=>{
                res.send({media:media,mediap:mediap,crediti:result3,appelli:result2});
            })
        })    
    })
})

//Modifica l'immagine del profilo
app.post('/modificaimmagine',upload.single('file') ,function (req, res) {
    db.query("SELECT * FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("UPDATE utente SET immagine=? WHERE id=?",[req.files.file.data,result[0].id],(err,result)=>{
            res.send(result);
        })
    })
})

//Inserisce un appello nel database
app.post('/inserisciappello',(req,res)=>{
    const materia=req.body.materia;
    const voto=req.body.voto;
    const crediti=req.body.crediti;
    var data=req.body.data;
    data=data.toString();

    let i=0;
    let numero=0;
    let voti=0;
    let votip=0;
    let crediti2=0;
    let media=0;
    let mediap=0;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT * FROM appello WHERE id=? AND materia=?",[result[0].id,materia],(err,result5)=>{
            if(result5[0]!=null){
                res.send("Appello già esistente");
            }else{
                db.query("INSERT INTO appello (id,materia,voto,crediti,data) VALUES (?,?,?,?,?)",[result[0].id,materia,voto,crediti,data],(err,result2)=>{
                    db.query("SELECT * FROM appello WHERE id=?",result[0].id,(err,result3)=>{
                        db.query("SELECT * FROM appello WHERE id=?",result[0].id,(err,result2)=>{
                            for(i=0;i<result2.length;i++){
                                if(result2[i].voto===0){
                                }else{
                                    numero++;
                                    voti=voti+result2[i].voto;
                                    votip=votip+(result2[i].voto*result2[i].crediti);
                                    crediti2=crediti2+result2[i].crediti;
                                }
                            }
                            media=voti/numero;
                            mediap=votip/crediti2;
                            db.query("SELECT SUM(crediti) as crediti FROM appello WHERE id=?",result[0].id,(err,result3)=>{
                                res.send({media:media,mediap:mediap,crediti:result3,appelli:result2});
                            })
                        })   
                    })
                })
            }
        })   
    })
})

//Rimuove l'appello dal database in base alla materia e l'utente
app.post('/rimuoviappello',(req,res)=>{
    const materia=req.body.materia;

    let i=0;
    let numero=0;
    let voti=0;
    let votip=0;
    let crediti=0;
    let media=0;
    let mediap=0;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("DELETE FROM appello WHERE id=? AND materia=?",[result[0].id,materia],(err,result2)=>{
            db.query("SELECT * FROM appello WHERE id=?",result[0].id,(err,result3)=>{
                for(i=0;i<result3.length;i++){
                    if(result3[i].voto===0){
                    }else{
                        numero++;
                        voti=voti+result3[i].voto;
                        votip=votip+(result3[i].voto*result3[i].crediti);
                        crediti=crediti+result3[i].crediti;
                    }
                }
                media=voti/numero;
                mediap=votip/crediti;
                db.query("SELECT SUM(crediti) as crediti FROM appello WHERE id=?",result[0].id,(err,result4)=>{
                    res.send({media:media,mediap:mediap,crediti:result4,appelli:result3});
                })
            })
        })
    })
})

//Carica un avviso nel database
app.post('/pubblicaavviso',(req,res)=>{
    const titolo=req.body.titoloavviso;
    const corpo=req.body.corpoavviso;
    //Per prendere la data e l'ora in cui viene caricato
    var dataOra = require('node-datetime');
    var data = dataOra.create();
    var formatta = data.format('Y-m-d H:M:S');

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("INSERT INTO avviso (id,titolo,corpo,data) VALUES (?,?,?,?)",[result[0].id,titolo,corpo,formatta],(err,result)=>{
            res.send(result)
        })
    })
})

//Recupera dal database tutti gli avvisi dell'utente della sessione corrente
app.get('/recuperaavvisipersonali',(req,res)=>{
    db.query("SELECT * FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT * FROM avviso WHERE id=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Rimuove l'avviso dal database corrispondente all'id
app.post('/rimuoviavviso',(req,res)=>{
    const idavviso=req.body.idavviso;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("DELETE FROM avviso WHERE idavviso=?",idavviso,(err,result2)=>{
            //Recupera la nuova lista avvisi
            db.query("SELECT * FROM avviso WHERE id=?",result[0].id,(err,result3)=>{
                res.send(result3)
            })
        })
    })
})

//Pubblica un messaggio nel forum
app.post('/pubblicaforum',(req,res)=>{
    const materia=req.body.materia;
    const messaggio=req.body.messaggio;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("INSERT INTO forum (idute,idmat,messaggio) VALUES (?,?,?)",[result[0].id,materia,messaggio],(err,result)=>{
            db.query("SELECT utente.nomeutente AS nome, forum.messaggio AS messaggio FROM forum INNER JOIN utente ON forum.idute=utente.id WHERE forum.idmat=?",materia,(err,result)=>{
                res.send(result)
            })
        })
    })
})

//Recupera i forum dal database
app.post('/recuperaforum',(req,res)=>{
    const materia=req.body.materia;
    
    db.query("SELECT utente.nomeutente AS nome, forum.messaggio AS messaggio FROM forum INNER JOIN utente ON forum.idute=utente.id WHERE forum.idmat=?",materia,(err,result)=>{
                res.send(result)
        })
})

//Carica un file nel database
app.post('/upload',upload.single('file') ,function (req, res) {
    if(req.files.file.name===null){
        res.send("Inserisci file");
    }else{
        db.query("SELECT * FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
            db.query("INSERT INTO documento (id,titolo,contenuto) VALUES (?,?,?)",[result[0].id,req.files.file.name,req.files.file.data],(err,result)=>{
                res.send(result);
            })
        })
    }
})

//Recupera dal database i files corrispondenti all'utente della sessione corrente
app.get('/recuperadocumentipersonali',(req,res)=>{
    db.query("SELECT * FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT * FROM documento WHERE id=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Rimuove dal database il file corrispondente all'id
app.post('/rimuovidocumento',(req,res)=>{
    const iddocumento=req.body.iddocumento;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("DELETE FROM documento WHERE iddocumento=?",iddocumento,(err,result2)=>{
            //ritorna la nuova lista documenti
            db.query("SELECT * FROM documento WHERE id=?",result[0].id,(err,result3)=>{
                res.send(result3)
            })
        })
    })
})

//Inserisce un nuovo test nel database
app.post('/createst',(req,res)=>{
    const titolo=req.body.titolotest;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT * FROM test WHERE titolo=?",titolo,(err,result2)=>{
            if(result2[0]!=null){
                res.send("Test già esistente")
            }else{
                db.query("INSERT INTO test (id,titolo) VALUES (?,?)",[result[0].id,titolo],(err,result)=>{
                    res.send(result)
                })
            }
        })
    })
})

//Inserisce una domanda nel database
app.post('/inseriscidomanda',(req,res)=>{
    const titolo=req.body.titolotest;
    const domanda=req.body.domanda;
    const opzione1=req.body.opzione1;
    const opzione2=req.body.opzione2;
    const opzione3=req.body.opzione3;
    const opzione4=req.body.opzione4;
    const rispostacorretta=req.body.rispostacorretta;

    db.query("SELECT idtest FROM test WHERE titolo=?",titolo,(err,result)=>{
        db.query("INSERT INTO domanda (idtest,domanda,opzione1,opzione2,opzione3,opzione4,rispostacorretta) VALUES (?,?,?,?,?,?,?)",[result[0].idtest,domanda,opzione1,opzione2,opzione3,opzione4,rispostacorretta],(err,result)=>{
                res.send(result)
        })    
    })
})

//Recupera i test dal database corrispondenti all'utente della sessione corrente
app.get('/recuperatestpersonali',(req,res)=>{
    db.query("SELECT * FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT * FROM test WHERE id=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Rimuove il test dal database corrispondente all'id
app.post('/rimuovitest',(req,res)=>{
    const idtest=req.body.idtest;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("DELETE FROM test WHERE idtest=?",idtest,(err,result2)=>{
            db.query("SELECT * FROM test WHERE id=?",result[0].id,(err,result3)=>{
                res.send(result3)
            })
        })   
    })
})

//Inserisce un evento nel database
app.post('/creaevento',(req,res)=>{
    var dataevento=req.body.dataevento;
    const evento=req.body.evento;
    dataevento=dataevento.toString().substring(0,10);
    const orainizio=dataevento+"T"+req.body.orainizio;
    const orafine=dataevento+"T"+req.body.orafine;
    
    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT * FROM agenda WHERE id=? AND evento=? AND orainizio=? AND orafine=?",[result[0].id,evento,orainizio,orafine],(err,result2)=>{
            if(result2.length>0){
                res.send({msg:"Evento già presente"})
            }else{
                db.query("INSERT INTO agenda (id,evento,orainizio,orafine) VALUES(?,?,?,?)",[result[0].id,evento,orainizio,orafine]);
                db.query("SELECT agenda.orainizio as startDate,agenda.orafine as endDate, agenda.evento as title FROM agenda INNER JOIN utente ON agenda.id=utente.id WHERE agenda.id=?",result[0].id,(err,result)=>{
                    res.send(result);
                })
            }
        })
    })
})

//Recupera gli eventi dal database
app.get('/recuperaeventi',(req,res)=>{
    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("SELECT agenda.orainizio as startDate,agenda.orafine as endDate, agenda.evento as title FROM agenda INNER JOIN utente ON agenda.id=utente.id WHERE agenda.id=?",result[0].id,(err,result)=>{
            res.send(result);
        })
    })
})

//Rimuove il test dal database corrispondente all'id
app.post('/rimuovievento',(req,res)=>{
    const titolo=req.body.titolo;
    const orainizio=req.body.oraInizio;
    const orafine=req.body.oraFine;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        db.query("DELETE FROM agenda WHERE id=? AND evento=? AND orainizio=? AND orafine=?",[result[0].id,titolo,orainizio,orafine],(err,result2)=>{
            db.query("SELECT evento as title, orainizio as startDate, orafine as endDate FROM agenda WHERE id=?",result[0].id,(err,result3)=>{
                res.send(result3)
            })
        })   
    })
})

//Aggiorna i dati del profilo sul database
app.post('/aggiornaprofilo',(req,res)=>{
    const nome=req.body.nome;
    const cognome=req.body.cognome;
    const matricola=req.body.matricola;
    const materie=req.body.materie;
    const password=req.body.password;
    const descrizione=req.body.descrizione;

    db.query("SELECT id FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        if(nome===""){
            res.send("Inserisci un nome")
        }else if(cognome===""){
            res.send("Inserisci un cognome")
        }else{
            db.query("SELECT * FROM studente WHERE matricola=? AND id!=?",[matricola,result[0].id],(err,result2)=>{
                if(result2.length>0||matricola===""){
                    res.send("Matricola già presente o non valida")
            }else{
                db.query("SELECT * FROM studente WHERE id=?",result[0].id,(err,result2)=>{
                    if(result2.length>0){
                    db.query("UPDATE studente SET matricola=? WHERE id=?",[matricola,result[0].id])
                    }else{
                        if(materie[0]!==""){
                        db.query("DELETE FROM insegnamento WHERE insid=?",result[0].id)
                        for(var i=0;i<materie.length;i++){
                            db.query("INSERT INTO insegnamento (insid,matkey) VALUES(?,?)",[result[0].id,materie[i]]);
                        }
                    }
                    }
                    if(password!==""){bcrypt.hash(password,saltRounds,(err,hash)=>{db.query("UPDATE utente SET nome=?,cognome=?,password=?,descrizione=? WHERE id=?",[nome,cognome,hash,descrizione,result[0].id],(err,result3)=>{
                        db.query("SELECT * FROM utente WHERE id=?",result[0].id,(err,result4)=>{
                            res.send(result4)
                        })
                    })})}else{db.query("UPDATE utente SET nome=?,cognome=?,descrizione=? WHERE id=?",[nome,cognome,descrizione,result[0].id],(err,result3)=>{
                                        db.query("SELECT * FROM utente WHERE id=?",result[0].id,(err,result4)=>{
                                            res.send(result4)
                                        })
                                    })  
                    }
                })
                }
            })    
        }   
    })
})

//Elimina la sessione corrente
app.post('/logout',(req,res)=>{
    req.session.destroy();
    res.send("sessione distrutta");
})

//Rimuove l'utente dal database
app.get('/eliminaaccount',(req,res)=>{
    db.query("DELETE FROM utente WHERE nomeutente=?",req.session.user[0].nomeutente,(err,result)=>{
        req.session.destroy();
        res.send(result);
    })
})

//Imposta la porta di comunicazione con il Client
app.listen(8000,()=>{
    console.log("server avviato");
})