import React,{useState,useEffect} from 'react'; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history' //Per i reindirizzamenti
import Axios from 'axios'; //Per le richieste tra Client e Server
import {Nav,Navbar,Image,Card,Spinner,Button} from 'react-bootstrap'; //Elementi di Bootstrap
import Search from '../components/barradiricerca';

const filtroAvvisi = (bacheca, query) => {
  if (!query) {
      return bacheca;
  }

  return bacheca.filter((avviso) => {
      const titoloAvviso = avviso.titolo.toLowerCase();
      return titoloAvviso.includes(query);
  });
};

function Bacheca(){
  //Costanti per la barra di navigazione modificate con set di useState
  const[immagineProfilo,setImmagineProfilo]=useState();
  //Costante per gli utenti seguiti
  const[seguiti,setSeguiti]=useState([{immagine:"",nome:"",cognome:""}]);
  //Costante per il contenuto della pagina modificata con set di useState
  const[bacheca,setBacheca]=useState([{titolo:"",corpo:"",nomeutente:"",data:""}]);
  //Costante per lo spinner modificata con set di useState
  const[spinner,setSpinner]=useState(true);

  //Barra di ricerca
  const { search } = window.location;
  const query = new URLSearchParams(search).get('s');
  const [searchQuery, setSearchQuery] = useState(query || '');
  const avvisiFiltrati = filtroAvvisi(bacheca, searchQuery);

  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route
  //Quando viene caricata la pagina
  useEffect(()=>{
    //Imposta l'immagine del profilo recuperandola dal database
    Axios.get("http://localhost:8000/recuperaprofilo").then((response)=>{
      setImmagineProfilo(Buffer.from(response.data.immagine.data, 'binary').toString('base64'));
    })
    //Recupera tutti gli utenti seguiti
    Axios.get('http://localhost:8000/utentiseguiti').then((response)=>{
      setSeguiti([...response.data]);
    })
    //Recupera tutti gli avvisi dal database e riempe il vettore di avvisi
    Axios.get("http://localhost:8000/recuperaavvisi").then((response)=>{
      if(response.data.length>0){
        setBacheca([...response.data]);
      }else{setSpinner(false)}
    })
  },[])
  
  //Esce dalla sessione e reindirizza alla pagina d'accesso
  const logout=()=>{
    Axios.post("http://localhost:8000/logout",).then((response)=>{
      history.push('/signin');
    })
  }

   //impaginazione----------------------

const[paginacorrente,setPaginacorrente]=useState(1);
const elementipagina=3;
const handleClick=(event)=>{
  setPaginacorrente(event.target.id)
}
const indiceUltimoElemento = paginacorrente * elementipagina;
const indicePrimoElemento = indiceUltimoElemento - elementipagina;
const elementicorrenti = avvisiFiltrati.slice(indicePrimoElemento, indiceUltimoElemento);

const renderElementi = elementicorrenti.map((item, index) => {
  return <Card key={index}>
  <div className="sfondocardavvisi">
    <Card.Title>{item.titolo}</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Pubblicato da: {item.nomeutente} il: {item.data}</Card.Subtitle>
  </div>
  <Card.Body>
    <Card.Text className="corpo">{item.corpo}</Card.Text>
  </Card.Body>
  </Card>
});

const numeriPagina = [];
    for (let i = 1; i <= Math.ceil(avvisiFiltrati.length / elementipagina); i++) {
      numeriPagina.push(i);
    }

    const renderNumeriPagina = numeriPagina.map(numero => {
      return (
        <Button className="numeropagina"
          key={numero}
          id={numero}
          onClick={handleClick}
        >
          {numero}
        </Button>
      );
    });


  return (
    <div className="sfondopagine">
      {/*Barra di navigazione*/}
      <Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Search>searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          </Search>
          <Nav className="col d-flex justify-content-end">
            <Nav.Link href="/segui">Segui</Nav.Link>
            <Nav.Link href="/forum">Forum</Nav.Link>
            <Nav.Link href="/documenti">Documenti</Nav.Link>
            <Nav.Link href="/test">Test</Nav.Link>
            {immagineProfilo!==undefined&&<a href="/profilo" className="immagineprofilo3">
              <Image width={30} height={30} src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo} alt="caricamento" roundedCircle/>
            </a>}
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      {bacheca[0].titolo===''&&spinner&&<Spinner animation="border" variant="warning" role="status"/>}
      
      {immagineProfilo!==undefined&&
        <div className="seguiti">
          <div className="utentiseguiti">Utenti seguiti:</div>
          {seguiti.map((seguito,index)=>
          <div key={index} className="utentiseguitidiv">{seguiti[0].immagine!==""&&<Image className="immagineprofilo" src={"data:image/jpg;charset=utf-8;base64," + Buffer.from(seguito.immagine.data,'binary').toString('base64')} roundedCircle/>}{seguito.nome} {seguito.cognome}</div>
          )}
          <div className="logout logoutpagine" onClick={logout}>Esci</div>
        </div>}
        {bacheca[0].titolo!==''&&<div><div className="avvisi">
        {renderElementi}  
        </div>
        <div className="numeripagina">{renderNumeriPagina}</div></div>}
    </div>
  )
}

export default withRouter(Bacheca)