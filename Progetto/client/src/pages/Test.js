import React,{useState,useEffect} from 'react'; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history'; //Per i reindirizzamenti
import Axios from 'axios'; //Per le richieste tra Client e Server
import {Nav,Navbar,Col,Image,Card,Button,Form,Modal,Spinner} from 'react-bootstrap'; //Elementi di Bootstrap
import Search from '../components/barradiricerca';

const filtroTest = (test, query) => {
  if (!query) {
      return test;
  }

  return test.filter((questionario) => {
      const questionarioTitolo = questionario.titolo.toLowerCase();
      return questionarioTitolo.includes(query);
  });
};

function Test(){
  //Costanti per la barra di navigazione modificate con set di useState
  const[immagineProfilo,setImmagineProfilo]=useState();
  //Costanti per il contenuto della pagina modificate con set di useState
  const[titolo,setTitolo]=useState('');
  const[test,setTest]=useState([{id:"",idtest:"",titolo:"",nomeutente:""}]);
  const[domande,setDomande]=useState([{id:"",idtest:"",domanda:"",opzione1:"",opzione2:"",opzione3:"",opzione4:"",rispostacorretta:""}]);
  //Vettore per salvare le risposte selezionate nei test
  var risposte=[]
  //Costanti per visualizzare i test e messaggi al completamento dei test
  const[isTest,setIsTest]=useState(true);
  const[show,setShow]=useState(false);
  const[msg,setMsg]=useState("");
  //Costante per gli utenti seguiti
  const[seguiti,setSeguiti]=useState([{immagine:"",nome:"",cognome:""}]);
  //Costante per lo spinner modificata con set di useState
  const[spinner,setSpinner]=useState(true);
  
  //Barra di ricerca
  const { search } = window.location;
  const query = new URLSearchParams(search).get('s');
  const [searchQuery, setSearchQuery] = useState(query || '');
  const testFiltrati = filtroTest(test, searchQuery);

  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route

  //Quando viene caricata la pagina
  useEffect(()=>{
    //Imposta l'immagine del profilo recuperandola dal database
    Axios.get("http://localhost:8000/recuperaprofilo").then((response)=>{
      setImmagineProfilo(Buffer.from(response.data.immagine.data, 'binary').toString('base64'));
    })
    //Recupera tutti gli utenti seguiti
    Axios.get('http://localhost:8000/utentiseguiti').then((response)=>{
      setSeguiti([...response.data]);
    })
    //Riempie il vettore dei test con i test recuperati dal database
    Axios.get("http://localhost:8000/recuperatest").then((response)=>{
      if(response.data.length>0){
        setTest([...response.data]);
      }else{setSpinner(false)}
    })
  },[])

  //Funzione che riempie il vettore domande con le domande corrispondenti al test selezionato recuperate dal database
  const recuperaDomande=(id,titolo)=>{
    setTitolo(titolo);
    Axios.post("http://localhost:8000/recuperadomande",{
      id:id
    }).then((response)=>{
      setIsTest(false);
      setDomande([...response.data]);
    })
  }

  //Esce dalla sessione e reindirizza alla pagina d'accesso
  const logout=()=>{
    Axios.post("http://localhost:8000/logout",).then((response)=>{
      history.push('/signin');
    })
  }


  //impaginazione----------------------

const[paginacorrente,setPaginacorrente]=useState(1);
const elementipagina=24;
const handleClick=(event)=>{
  setPaginacorrente(event.target.id)
}
const indiceUltimoElemento = paginacorrente * elementipagina;
const indicePrimoElemento = indiceUltimoElemento - elementipagina;
const elementicorrenti = testFiltrati.slice(indicePrimoElemento, indiceUltimoElemento);

const renderElementi = elementicorrenti.map((item, index) => {
  return <Card className="carddocumenti" key={index}>
  <Card.Body>
    <Card.Title>{item.titolo}</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Pubblicato da: {item.nomeutente}</Card.Subtitle>
    <Button onClick={()=>{recuperaDomande(item.idtest,item.titolo)}}>Avvia Test</Button>
  </Card.Body>
</Card>
});

const numeriPagina = [];
    for (let i = 1; i <= Math.ceil(testFiltrati.length / elementipagina); i++) {
      numeriPagina.push(i);
    }

    const renderNumeriPagina = numeriPagina.map(numero => {
      return (
        <Button className="numeropagina"
          key={numero}
          id={numero}
          onClick={handleClick}
        >
          {numero}
        </Button>
      );
    });


  return (
    <div className="sfondopagine">
      {/*Barra di navigazione*/}
      <Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Search>searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          </Search>
          <Nav className="col d-flex justify-content-end">
            <Nav.Link href="/segui">Segui</Nav.Link>
            <Nav.Link href="/bacheca">Bacheca</Nav.Link>
            <Nav.Link href="/forum">Forum</Nav.Link>
            <Nav.Link href="/documenti">Documenti</Nav.Link>
            {immagineProfilo!==undefined&&<a href="/profilo" className="immagineprofilo3">
              <Image width={30} height={30} src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo} alt="caricamento" roundedCircle/>
            </a>}
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      {test[0].idtest===''&&spinner&&<Spinner animation="border" variant="warning" role="status"/>}
      {immagineProfilo!==undefined&&<div className="seguiti">
        <div className="box3">
          <div className="utentiseguiti">Utenti seguiti:</div>
          {seguiti.map((seguito,index)=>
          <div key={index} className="utentiseguitidiv">{seguiti[0].immagine!==""&&<Image className="immagineprofilo" src={"data:image/jpg;charset=utf-8;base64," + Buffer.from(seguito.immagine.data,'binary').toString('base64')} roundedCircle/>}{seguito.nome} {seguito.cognome}</div>
          )}
          <div className="logout logoutpagine" onClick={logout}>Esci</div>
        </div>
        </div>}
      {/*Pagina con tutti i test, visualizzata solo se non è già stato selezionato un test (quando isTest è true)*/}
      {isTest&&<div>
        {test[0].id!==''&&<div><div className="documenti">
          {renderElementi}
        </div>
        <div className="numeripagina">{renderNumeriPagina}</div></div>}
      </div>}

      {/*Pagina con le domande del test, visualizzata solo se è stato selezionato un test (quando isTest è false)*/}
      {isTest===false&&<div className="documenti">
        <div className="domande">
        <h1>{titolo}</h1>
        {domande.map((domanda,index)=><Form key={index}>
          <fieldset>
            <Form.Group  onChange={(event)=>{risposte[index]=event.target.value}}>
              <div>• {domanda.domanda}</div>
              <Col sm={10}>
                <Form.Check
                  type="radio"
                  label={domanda.opzione1}
                  name="formHorizontalRadios"
                  id={index+"prima"}
                  value={domanda.opzione1}
                />
                <Form.Check
                  type="radio"
                  label={domanda.opzione2}
                  name="formHorizontalRadios"
                  id={index+"seconda"}
                  value={domanda.opzione2}
                />
                <Form.Check
                  type="radio"
                  label={domanda.opzione3}
                  name="formHorizontalRadios"
                  id={index+"terza"}
                  value={domanda.opzione3}
                />
                <Form.Check
                  type="radio"
                  label={domanda.opzione4}
                  name="formHorizontalRadios"
                  id={index+"quarta"}
                  value={domanda.opzione4}
                />
              </Col>
            </Form.Group>
          </fieldset>
        </Form>)}
        <Button onClick={()=>{
          var i=0;
          var j=0;
          for(i=0;i<risposte.length;i++){
          if(risposte[i]===domande[i].rispostacorretta){
                  j++
                }};
          setMsg(j);
          setShow(true);}}
        >Conferma</Button>
        {/*Modale che viene visualizzata solo quando si confermano le risposte, quando show è true*/}
        <Modal show={show} onHide={()=>{setShow(false);window.location.reload()}}>
          <Modal.Header closeButton>
            <Modal.Title>Test completato!</Modal.Title>
          </Modal.Header>
          <Modal.Body>Risposte corrette: {msg}</Modal.Body>
          <Modal.Footer></Modal.Footer>
        </Modal>
      </div>
      </div>}
    </div>
  )
}

export default withRouter(Test)