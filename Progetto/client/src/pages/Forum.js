import React,{useState,useEffect} from 'react'; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history' //Per i reindirizzamenti
import Axios from 'axios'; //Per le richieste tra Client e Server
import {Nav,Navbar,Image,Card,Spinner,Button,Form, ButtonGroup} from 'react-bootstrap'; //Elementi di Bootstrap
import Search from '../components/barradiricerca';

const filtroForum = (materieArray, query) => {
  if (!query) {
      return materieArray;
  }

  return materieArray.filter((materia) => {
      const materiaLabel = materia.label.toLowerCase();
      return materiaLabel.includes(query);
  });
};

function Forum(){
  //Costanti per la barra di navigazione modificate con set di useState
  const[immagineProfilo,setImmagineProfilo]=useState();
  //Costante per gli utenti seguiti
  const[seguiti,setSeguiti]=useState([{immagine:"",nome:"",cognome:""}]);
  const[show,setShow]=useState(true);
  //Costante per lo spinner modificata con set di useState
  const[spinner,setSpinner]=useState(true);
  //Costante per le materie dei forum modificata con set di useState
  const[materieArray,setMaterieArray]=useState([{key:'', label:''}]);
  
  const[forum,setForum]=useState([{autore:'', messaggio:''}]);
  const[materia,setMateria]=useState("");
  const[messaggio,setMessaggio]=useState("");

  //Barra di ricerca
  const { search } = window.location;
  const query = new URLSearchParams(search).get('s');
  const [searchQuery, setSearchQuery] = useState(query || '');
  const forumFiltrati = filtroForum(materieArray, searchQuery);

  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route
  //Quando viene caricata la pagina
  useEffect(()=>{
    //Imposta l'immagine del profilo recuperandola dal database
    Axios.get("http://localhost:8000/recuperaprofilo").then((response)=>{
      setImmagineProfilo(Buffer.from(response.data.immagine.data, 'binary').toString('base64'));
    })
    //Recupera tutti gli utenti seguiti
    Axios.get('http://localhost:8000/utentiseguiti').then((response)=>{
      setSeguiti([...response.data]);
    })
    //Riempie il vettore delle materie con le materie del database
    Axios.get("http://localhost:8000/recuperamaterie").then((response)=>{
        if(response.data.length>0){
            setMaterieArray([...response.data]);
            setSpinner(false);
        }else{setSpinner(false)}
    })
  },[])
  
  const pubblicaForum=(materia)=>{
    Axios.post("http://localhost:8000/pubblicaforum",{
        materia:materia,
        messaggio:messaggio
    }).then((response)=>{
        if(response.data.length>0){
            setForum([...response.data]);
            setSpinner(false);
        }else{setSpinner(false)}
    })
  }

  const recuperaforum=(materia)=>{
    Axios.post("http://localhost:8000/recuperaforum",{
        materia:materia
    }).then((response)=>{
        if(response.data.length>0){
            setForum([...response.data]);
            setSpinner(false);
        }else{setSpinner(false)}
    })
  }

  //Esce dalla sessione e reindirizza alla pagina d'accesso
  const logout=()=>{
    Axios.post("http://localhost:8000/logout",).then((response)=>{
      history.push('/signin');
    })
  }

//impaginazione----------------------

const[paginacorrente,setPaginacorrente]=useState(1);
const elementipagina=18;
const handleClick=(event)=>{
  setPaginacorrente(event.target.id)
}
const indiceUltimoElemento = paginacorrente * elementipagina;
const indicePrimoElemento = indiceUltimoElemento - elementipagina;
const elementicorrenti = forumFiltrati.slice(indicePrimoElemento, indiceUltimoElemento);
const elementicorrenti2 = forum.slice(indicePrimoElemento, indiceUltimoElemento);


const renderElementi = elementicorrenti.map((item, index) => {
  return <Card key={index}> 
  <Card.Body>
    <Card.Title>{item.label}</Card.Title>
    <Card.Subtitle className="mb-2 text-muted"></Card.Subtitle>
    <Button onClick={()=>{setShow(false);recuperaforum(item.key);setMateria(item.key)}}>Entra</Button>
  </Card.Body>
  </Card>
});

const renderElementi2 = elementicorrenti2.map((item, index) => {
  return <Card key={index}>
  {item.nome!==""&&<Card.Body>
    <Card.Subtitle>Inviato da: {item.nome}</Card.Subtitle>
    <Card.Text>{item.messaggio}</Card.Text>
  </Card.Body>}
  {item.nome===""&&<Card.Body>Non è presente nessun messaggio</Card.Body>}
  </Card>
});


const numeriPagina = [];
    for (let i = 1; i <= Math.ceil(forumFiltrati.length / elementipagina); i++) {
      numeriPagina.push(i);
    }

    const renderNumeriPagina = numeriPagina.map(numero => {
      return (
        <Button className="numeropagina"
          key={numero}
          id={numero}
          onClick={handleClick}
        >
          {numero}
        </Button>
      );
    });

    const numeriPagina2 = [];
    for (let i = 1; i <= Math.ceil(forum.length / elementipagina); i++) {
      numeriPagina2.push(i);
    }

    const renderNumeriPagina2 = numeriPagina2.map(numero => {
      return (
        <Button className="numeropagina"
          key={numero}
          id={numero}
          onClick={handleClick}
        >
          {numero}
        </Button>
      );
    });

    

  return (
    <div className="sfondopagine">
      {/*Barra di navigazione*/}
      <Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Search>searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          </Search>
          <Nav className="col d-flex justify-content-end">
            <Nav.Link href="/segui">Segui</Nav.Link>
            <Nav.Link href="/bacheca">Bacheca</Nav.Link>
            <Nav.Link href="/documenti">Documenti</Nav.Link>
            <Nav.Link href="/test">Test</Nav.Link>
            {immagineProfilo!==undefined&&<a href="/profilo" className="immagineprofilo3">
              <Image width={30} height={30} src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo} alt="caricamento" roundedCircle/>
            </a>}
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      {materieArray[0].label===''&&spinner&&<Spinner animation="border" variant="warning" role="status"/>}
      
      {immagineProfilo!==undefined&&
        <div className="seguiti">
          <div className="utentiseguiti">Utenti seguiti:</div>
          {seguiti.map((seguito,index)=>
          <div key={index} className="utentiseguitidiv">{seguiti[0].immagine!==""&&<Image className="immagineprofilo" src={"data:image/jpg;charset=utf-8;base64," + Buffer.from(seguito.immagine.data,'binary').toString('base64')} roundedCircle/>}{seguito.nome} {seguito.cognome}</div>
          )}
          <div className="logout logoutpagine" onClick={logout}>Esci</div>
        </div>}
        {materieArray[0].label!==''&&show&&<div><div className="documenti">
        
            {renderElementi}
          
        </div>
        <div className="numeripagina">{renderNumeriPagina}</div></div>}
        {show===false&&<div><div className="forum">
          
          {renderElementi2}
          
          {show===false&&<Form>
            <Form.Label>Pubblica un nuovo messaggio</Form.Label>
                <Form.Control className="areatesto" placeholder="Inserisci messaggio..." as="textarea" rows={10} onChange={(e)=>{setMessaggio(e.target.value)}}/>
                <ButtonGroup className="forumgroup">
                    <Button onClick={()=>{pubblicaForum(materia)}}>Invia</Button>
                    <Button onClick={(e)=>{setShow(true);setForum([...[{nome:"",messaggio:""}]])}}>Torna indietro</Button>
                </ButtonGroup>
            </Form>}
            
        </div>
        <div className="numeripagina">{renderNumeriPagina2}</div></div>}
    </div>
  )
}

export default withRouter(Forum)