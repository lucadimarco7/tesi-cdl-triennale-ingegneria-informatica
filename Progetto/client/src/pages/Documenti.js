import React,{useState,useEffect} from 'react'; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history'; //Per i reindirizzamenti
import Axios from 'axios'; //Per le richieste tra Client e Server
import {Nav,Navbar,Image,Button,Card,Spinner} from 'react-bootstrap'; //Elementi di Bootstrap
import Search from '../components/barradiricerca';

const FileDownload = require('js-file-download'); //Per scaricare i documenti
const filtraFile = (files, query) => {
  if (!query) {
      return files;
  }

  return files.filter((file) => {
      const titoloFile = file.titolo.toLowerCase();
      return titoloFile.includes(query);
  });
};

function Documenti(){
  //Costanti per la barra di navigazione modificate con set di useState
  const[immagineProfilo,setImmagineProfilo]=useState();
  //Costante per il contenuto della pagina modificata con set di useState
  const[files,setFiles]=useState([{id:'',iddocumento:"",titolo:'',contenuto:"",nomeutente:""}]);
  //Costante per lo spinner modificata con set di useState
  const[spinner,setSpinner]=useState(true);
  //Costante per gli utenti seguiti
  const[seguiti,setSeguiti]=useState([{immagine:"",nome:"",cognome:""}]);

  //Barra di ricerca
  const { search } = window.location;
  const query = new URLSearchParams(search).get('s');
  const [searchQuery, setSearchQuery] = useState(query || '');
  const fileFiltrati = filtraFile(files, searchQuery);
  

  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route
  //Quando viene caricata la pagina
  useEffect(()=>{
    //Imposta l'immagine del profilo recuperandola dal database
    Axios.get("http://localhost:8000/recuperaprofilo").then((response)=>{
      setImmagineProfilo(Buffer.from(response.data.immagine.data, 'binary').toString('base64'));
    })
    //Riempie il vettore dei files con i files recuperati dal database
    Axios.get("http://localhost:8000/files").then((response)=>{
      if(response.data.length>0){
        setFiles([...response.data]);
      }else{setSpinner(false);}
    })
    //Recupera tutti gli utenti seguiti
    Axios.get('http://localhost:8000/utentiseguiti').then((response)=>{
      setSeguiti([...response.data]);
    })
  },[])

  //Funzione che al click del pulsante download scarica il file corrispondente
  function download(index,titolo){
    Axios.get('http://localhost:8000/download',{
      responseType:'blob',
      params:{index:index}
    }).then((response) => {
      FileDownload(response.data,titolo);
    });
  }

  //Esce dalla sessione e reindirizza alla pagina d'accesso
  const logout=()=>{
    Axios.post("http://localhost:8000/logout",).then((response)=>{
      history.push('/signin');
    })
  }

  //impaginazione----------------------

const[paginacorrente,setPaginacorrente]=useState(1);
const elementipagina=18;
const handleClick=(event)=>{
  setPaginacorrente(event.target.id)
}
const indiceUltimoElemento = paginacorrente * elementipagina;
const indicePrimoElemento = indiceUltimoElemento - elementipagina;
const elementicorrenti = fileFiltrati.slice(indicePrimoElemento, indiceUltimoElemento);

const renderElementi = elementicorrenti.map((item, index) => {
  return <Card key={index}>
          <Card.Body>
            <Card.Title>{item.titolo}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">Pubblicato da: {item.nomeutente}</Card.Subtitle>
            <Button onClick={(event)=>{download(item.iddocumento,item.titolo)}}>Scarica</Button>
          </Card.Body>
        </Card>
});

const numeriPagina = [];
    for (let i = 1; i <= Math.ceil(fileFiltrati.length / elementipagina); i++) {
      numeriPagina.push(i);
    }

    const renderNumeriPagina = numeriPagina.map(numero => {
      return (
        <Button className="numeropagina"
          key={numero}
          id={numero}
          onClick={handleClick}
        >
          {numero}
        </Button>
      );
    });


  return (
    <div className="sfondopagine">
      {/*Barra di navigazione*/}
      <Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Search>searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          </Search>
          <Nav className="col d-flex justify-content-end">
            <Nav.Link href="/segui">Segui</Nav.Link>
            <Nav.Link href="/bacheca">Bacheca</Nav.Link>
            <Nav.Link href="/forum">Forum</Nav.Link>
            <Nav.Link href="/test">Test</Nav.Link>
            {immagineProfilo!==undefined&&<a href="/profilo" className="immagineprofilo3">
              <Image width={30} height={30} src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo} alt="caricamento" roundedCircle/>
            </a>}
          </Nav>
        </Navbar.Collapse>
      </Navbar>


      {files[0].titolo===''&&spinner&&<Spinner animation="border" variant="warning" role="status"/>}

      {immagineProfilo!==undefined&&<div className="seguiti">
          <div className="utentiseguiti">Utenti seguiti:</div>
          {seguiti.map((seguito,index)=>
          <div key={index} className="utentiseguitidiv">{seguiti[0].immagine!==""&&<Image className="immagineprofilo" src={"data:image/jpg;charset=utf-8;base64," + Buffer.from(seguito.immagine.data,'binary').toString('base64')} roundedCircle/>}{seguito.nome} {seguito.cognome}</div>
          )}
          <div className="logout logoutpagine" onClick={logout}>Esci</div>
        </div>}
        {files[0].titolo!==''&&<div><div className="documenti">
        {renderElementi}
      </div>
      <div className="numeripagina">{renderNumeriPagina}</div></div>}
    </div>
  )
}

export default withRouter(Documenti)