import React, { useState } from 'react'; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history'; //Per i reindirizzamenti
import {ButtonGroup,Button,Image} from 'react-bootstrap'; //Elementi di Bootstrap
import image from '../images/logo3.png'; //Logo del sito
import image2 from '../images/immaginehome2.png';

function Home(){
    //Carica la nuova pagina al cambio di route
    const history=createHistory({forceRefresh:true});
    const[obiettivo,setObiettivo]=useState(true);
    const[offre,setOffre]=useState(false);
    const[analisi,setAnalisi]=useState(false);

    return(
        <div className="sfondohome">
            <div className="intestazionehome">
                <Image className="immaginehome" src={image} fluid />
                <span className="schedehome">
                    <span onClick={()=>{setAnalisi(false);setOffre(false);setObiettivo(true)}}>L'Obiettivo</span>
                    <span onClick={()=>{setAnalisi(false);setOffre(true);setObiettivo(false)}}>Cosa Offre</span>
                    <span onClick={()=>{setAnalisi(true);setOffre(false);setObiettivo(false)}}>L'analisi</span>
                </span>
            </div>
            {obiettivo&&<div>
            <h1 className="titolohome">L'Obiettivo</h1>
            <p>
                Il principale obiettivo di questa piattaforma è quello di migliorare,
                puntando alla semplicità di utilizzo, 
                la relazione virtuale tra studenti e
                professori e tra studenti stessi.
                Offrire delle funzionalità per migliorare la gestione della propria carriera universitaria,
                tenendo traccia dei propri progressi, delle novità e
                dei documenti condivisi dagli utenti alla comunità.
                </p></div>}
            {offre&&<div>
            <h1 className="titolohome">Cosa Offre</h1>
            <p>
                Un qualsiasi utente registrato alla piattaforma può scegliere gli utenti da seguire,
                siano essi docenti o studenti.
                Ha la possibilità di gestire il proprio profilo, aggiornando la propria immagine o la descrizione,
                oltre che gestire i documenti caricati.
                I docenti possono pubblicare avvisi per i loro studenti o creare dei test per loro.
                Di conseguenza gli studenti hanno la possibilità di svolgere tali test,
                possono tener traccia dei loro progressi universitari registrando gli esami nel libretto e
                segnarsi gli orari di eventi o lezioni.
                </p></div>}
            {analisi&&<div>
            <h1 className="titolohome">L'Analisi</h1>
            <p>
                Sono presenti diverse piattaforme che offrono servizi simili tra loro e a questa.
                L'obiettivo fondamentale di questa, però, è la semplicità, l'immediatezza e racchiudere
                più funzionalità in un'unica piattaforma.    
            </p></div>}
            <ButtonGroup size="lg" className="mb-2">
                <Button className="pulsantihome" variant="primary" onClick={()=>{history.push('/signin')}}>Accedi</Button>
                <Button className="pulsantihome" variant="primary" onClick={()=>{history.push('/signup')}}>Registrati</Button>
            </ButtonGroup>     
            <Image className="immaginehome2" src={image2}/>   
        </div>
    )
}

export default withRouter(Home)