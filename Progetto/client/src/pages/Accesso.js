import React,{useState,useEffect} from "react"; //React-App
import {withRouter} from "react-router-dom"; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history'; //Per i reindirizzamenti
import Axios from 'axios'; //Per le richieste tra Client e Server
import 'bootstrap/dist/css/bootstrap.min.css'; //Libreria Bootstrap
import {Form,Button,Nav,Image} from 'react-bootstrap'; //Elementi di Bootstrap
import image from '../images/logo3.png'; //Logo sito

function Accesso() {
  //Costanti del form di accesso modificate con set di useState 
  const[nomeutente,setNomeutente]=useState('');
  const[password,setPassword]=useState('');

  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route
  //Costanti di messaggi di errore in caso di compilazione errata del form
  const[msg,setMsg]=useState('');
  const[erroreNome,setErroreNome]=useState();

  Axios.defaults.withCredentials=true; //Serve per leggere la sessione e i cookie prima delle richieste Axios
  
  //Quando viene caricata la pagina
  useEffect(()=>{
    //Verifica se c'è una sessione attiva
    Axios.get("http://localhost:8000/login").then((response)=>{
      if(response.data.loggedIn===true){
        history.push('/bacheca')
      }
    })
  },[history])

  //Login  dell'utente, inviando i dati del form e ritornado un messaggio di successo o gli eventuali errori
  const login=()=>{
    Axios.post("http://localhost:8000/login",{
      nomeutente:nomeutente,
      password:password
    }).then((response)=>{
      if(response.data.loggedIn===false){
        setMsg(response.data.msg);
        setErroreNome(true);
        }else{
        history.push('/bacheca'); //Reindirizza alla bacheca nel caso di successo
      }
    })
  }

  return (
    <div className="sfondoaccesso">
      <div className="containeraccesso">

            <Nav className="navregistrazione">
            <Image className="immaginehome" src={image}/>
              <Nav.Item className="col d-flex justify-content-end">
                <Nav.Link href="/">Home</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="disabled" disabled>Accedi</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link href="/signup">Registrati</Nav.Link>
              </Nav.Item>
            </Nav>
            <Form id="form" className="formregistrazione" onSubmit={(e)=>{e.preventDefault()}}>
              <Form.Group controlId="formGridName">
                <Form.Label>Nome Utente</Form.Label>
                <Form.Control type="text" isInvalid={erroreNome} placeholder="Inserisci nome utente..." onChange={(event)=>{setNomeutente(event.target.value);}}/>
                <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
                </Form.Group>
              <Form.Group className="formlogin" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Inserisci password..." onChange={(event)=>{setPassword(event.target.value);}}/>
                </Form.Group>
              <Form.Group>
                <Button variant="primary" type="submit" className="pulsanteregistrazione" onClick={login}>Accedi</Button>
              </Form.Group>
            </Form>
      </div>
    </div>
  )
}

export default withRouter(Accesso);
