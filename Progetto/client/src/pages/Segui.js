import React,{useState,useEffect} from 'react'; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history' //Per i reindirizzamenti
import Axios from 'axios'; //Per le richieste tra Client e Server
import {Nav,Navbar,Image,Card,Button,Figure, ButtonGroup,Col,Tabs,Tab,Spinner} from 'react-bootstrap'; //Elementi di Bootstrap
import Search from '../components/barradiricerca';

const filtroUtenti = (listaUtenti, query) => {
  if (!query) {
      return listaUtenti;
  }

  return listaUtenti.filter((utente) => {
      const utenteNome = utente.nome.toLowerCase();
      const utenteCognome = utente.cognome.toLowerCase();
      return utenteNome.includes(query)||utenteCognome.includes(query);
  });
};

function Segui(){
  //Costanti per la barra di navigazione modificate con set di useState
  const[immagineProfilo,setImmagineProfilo]=useState();
  const[immagineProfilo1,setImmagineProfilo1]=useState();
  const[isStudente,setIsStudente]=useState();
  const[id,setId]=useState();
  const[nome,setNome]=useState('');
  const[cognome,setCognome]=useState('');
  const[email,setEmail]=useState('');
  const[matricola,setMatricola]=useState('');
  const[descrizione,setDescrizione]=useState('');
  const[materie,setMaterie]=useState([{label:""}]);
  const[seguito,setSeguito]=useState();
  const[nomeutente1,setNomeutente1]=useState('');
  const[listaUtenti,setListaUtenti]=useState([{id:"",nome:"",cognome:"",nomeutente:"",email:"",immagine:""}]);
  const[show,setShow]=useState(true);
  //Costante per gli utenti seguiti
  const[seguiti,setSeguiti]=useState([{immagine:"",nome:"",cognome:""}]);

  //Barra di ricerca
  const { search } = window.location;
  const query = new URLSearchParams(search).get('s');
  const [searchQuery, setSearchQuery] = useState(query || '');
  const utentiFiltrati = filtroUtenti(listaUtenti, searchQuery);

  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route
  //Quando viene caricata la pagina
  useEffect(()=>{ 
    //Imposta l'immagine del profilo recuperandola dal database
    Axios.get("http://localhost:8000/recuperaprofilo").then((response)=>{
      setImmagineProfilo(Buffer.from(response.data.immagine.data, 'binary').toString('base64'));
    })
    //Recupera la lista di tutti gli utenti registrati
    Axios.get("http://localhost:8000/recuperautenti").then((response)=>{
      setListaUtenti([...response.data]);
    })
    //Recupera tutti gli utenti seguiti
    Axios.get('http://localhost:8000/utentiseguiti').then((response)=>{
      setSeguiti([...response.data]);
    })
  },[])
  
  //Esce dalla sessione e reindirizza alla pagina d'accesso
  const logout=()=>{
    Axios.post("http://localhost:8000/logout",).then((response)=>{
      history.push('/signin');
    })
  }

  //Segue l'utente cliccato
  const segui=(evento)=>{
    Axios.post('http://localhost:8000/segui',{
      id:evento
    }).then((response)=>{
    })
  }

  //Smette di seguire l'utente cliccato
  const rimuovisegui=(evento)=>{
    Axios.post('http://localhost:8000/rimuovisegui',{
      id:evento
    }).then((response)=>{
      setSeguito(false);
    })
  }

  //Visualizza il profilo dell'utente cliccato
  const profilo=(evento)=>{
    Axios.post("http://localhost:8000/recuperaprofilo",{
      id:evento
    }).then((response)=>{
      setId(response.data.id);
      setImmagineProfilo1(Buffer.from(response.data.immagine.data,'binary').toString('base64'));
      setNome(response.data.nome);
      setCognome(response.data.cognome);
      setNomeutente1(response.data.nomeutente);
      setEmail(response.data.email);
      setMatricola(response.data.matricola);
      setDescrizione(response.data.descrizione);
      setIsStudente(response.data.isStudente);
      if(response.data.isStudente===false){
      setMaterie([...response.data.materie]);}
      setSeguito(response.data.segui);
    })
  }

  const utentiseguiti=()=>{
    Axios.get('http://localhost:8000/utentiseguiti').then((response)=>{
      setSeguiti([...response.data]);
    })
  }

 //impaginazione----------------------

 const[paginacorrente,setPaginacorrente]=useState(1);
 const elementipagina=14;
 const handleClick=(event)=>{
   setPaginacorrente(event.target.id)
 }
 const indiceUltimoElemento = paginacorrente * elementipagina;
 const indicePrimoElemento = indiceUltimoElemento - elementipagina;
 const elementicorrenti = utentiFiltrati.slice(indicePrimoElemento, indiceUltimoElemento);
 
 const renderElementi = elementicorrenti.map((item, index) => {
   return <Card key={index}>
   <div className="sfondocardsegui"></div>
   <Card.Body>
     {item.immagine.data!==undefined&&<Image src={"data:image/jpg;charset=utf-8;base64," + Buffer.from(item.immagine.data,'binary').toString('base64')}/>}
     <Card.Title>{item.nome} {item.cognome}</Card.Title>
     <Card.Subtitle className="mb-2 text-muted">📛 {item.nomeutente}</Card.Subtitle>
     <Card.Subtitle className="mb-2 text-muted">📧 {item.email}</Card.Subtitle>
     <Button variant="warning" value={item.id} onClick={(event)=>{profilo(event.target.value);setShow(false)}}>Vai al profilo</Button>
   </Card.Body>
 </Card>
 });
 
 const numeriPagina = [];
     for (let i = 1; i <= Math.ceil(utentiFiltrati.length / elementipagina); i++) {
       numeriPagina.push(i);
     }
 
     const renderNumeriPagina = numeriPagina.map(numero => {
       return (
         <Button className="numeropagina"
           key={numero}
           id={numero}
           onClick={handleClick}
         >
           {numero}
         </Button>
       );
     });
 



  return (
    <div className="sfondopagine">
      {/*Barra di navigazione*/}
      {show&&<Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Search>searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          </Search>
          <Nav className="col d-flex justify-content-end">
            <Nav.Link href="/bacheca">Bacheca</Nav.Link>
            <Nav.Link href="/forum">Forum</Nav.Link>
            <Nav.Link href="/documenti">Documenti</Nav.Link>
            <Nav.Link href="/test">Test</Nav.Link>
            {immagineProfilo!==undefined&&<a href="/profilo" className="immagineprofilo3">
              <Image width={30} height={30} src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo} alt="caricamento" roundedCircle/>
            </a>}
          </Nav>
        </Navbar.Collapse>
      </Navbar>}

      {immagineProfilo===undefined&&<Spinner animation="border" variant="warning" role="status"/>}
      
      {show&&immagineProfilo!==undefined&&<div><div className="segui">
      <div className="seguiti">
          <div className="utentiseguiti">Utenti seguiti:</div>
          {seguiti.map((seguito,index)=>
          <div key={index} className="utentiseguitidiv">{seguiti[0].immagine!==""&&<Image className="immagineprofilo" src={"data:image/jpg;charset=utf-8;base64," + Buffer.from(seguito.immagine.data,'binary').toString('base64')} roundedCircle/>}{seguito.nome} {seguito.cognome}</div>
          )}
          <div className="logout logoutpagine" onClick={logout}>Esci</div>
        </div>
        {renderElementi}
      </div>
      <div className="numeripagina">{renderNumeriPagina}</div></div>}
      {show===false&&immagineProfilo1!==undefined&&<Navbar className="navbarsegui" collapseOnSelect expand="lg">
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="col d-flex justify-content-end">
            <Nav.Link href="/bacheca">Bacheca</Nav.Link>
            <Nav.Link href="/forum">Forum</Nav.Link>
            <Nav.Link href="/documenti">Documenti</Nav.Link>
            <Nav.Link href="/test">Test</Nav.Link>
            {immagineProfilo!==undefined&&<a href="/profilo" className="immagineprofilo3">
              <Image width={30} height={30} src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo} alt="caricamento" roundedCircle/>
            </a>}
          </Nav>
        </Navbar.Collapse>
      </Navbar>}
      {show===false&&immagineProfilo1!==undefined&&<div className="profilo">
        <Figure className="figura">
          <Image className="immagineprofilo2" width={350} height={350} src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo1} alt="caricamento" roundedCircle/>
        </Figure>
        <div className="infoprofilo infoprofilosegui">
          <div className="nomeprofilo">{nome} {cognome}</div>
          <div>📛 {nomeutente1}</div>
          {/*Visualizzata solo se è uno studente, quando isStudente è true*/}
          {isStudente && <div>👨🏻‍🎓  {matricola}</div>}
          {/*Visualizzata solo se è un insegnante, quando isStudente è false*/}
          {isStudente===false && <div>📚 {materie.map((materia,index)=><span key={index}>{materia.label}<br></br></span>)}</div>}
          <div>📧 {email}</div>
          <ButtonGroup>
            {seguito===false&&<Button variant="light" value={id} onClick={(event)=>{segui(event.target.value);setSeguito(true)}}>Segui</Button>}
            {seguito&&<Button variant="light" value={id} onClick={(event)=>{rimuovisegui(event.target.value);setSeguito(true)}}>Non seguire più</Button>}
            <Button variant="light" onClick={()=>{utentiseguiti();setShow(true)}}>Torna indietro</Button>
          </ButtonGroup>
          <div className="logout" onClick={logout}>Esci</div>
        </div>
        <Col className="colprofilo">
        <Tabs defaultActiveKey="descrizione" className="gestioneprofilo" id="uncontrolled-tab-example">
          <Tab eventKey="descrizione" className="descrizione" title="descrizione">
            <div>{descrizione}</div>
          </Tab>
        </Tabs>
        </Col>
        </div>}
    </div>
  )
}

export default withRouter(Segui)