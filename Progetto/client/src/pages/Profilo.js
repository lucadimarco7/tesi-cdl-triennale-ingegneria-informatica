import React,{useState,useEffect} from 'react'; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history'; //Per i reindirizzamenti
import Axios from 'axios'; //Per le richieste tra Client e Server
import {Nav,Navbar,Col,Image,Figure,Tabs,Tab,Form,Button,Modal,Table,Alert,ButtonGroup,Row,Spinner} from 'react-bootstrap'; //Elementi di Bootstrap
import DatePicker from 'react-datepicker'; //Per selezionare una data dal calendario
import TimePicker from 'react-time-picker'; //Per selezionare un orario
import "react-datepicker/dist/react-datepicker.css"; //Libreria di datepicker
import Paper from '@material-ui/core/Paper';
import {ViewState} from '@devexpress/dx-react-scheduler';
import {Scheduler,Toolbar,WeekView,DateNavigator,Appointments,AppointmentTooltip} from '@devexpress/dx-react-scheduler-material-ui';
import moment from 'moment';
import DropdownMultiselect from 'react-multiselect-dropdown-bootstrap'; //Menù a scelta multipla di Bootstrap
import Resizer from "react-image-file-resizer"; //Ridimensionare immagini


function Profilo(){
  //Costanti per la barra di navigazione modificate con set di useState
  const[nomeutente,setNomeutente]=useState('');
  const[immagineProfilo,setImmagineProfilo]=useState();
  //Costanti per le informazioni del profilo modificate con set di useState
  const[nome,setNome]=useState('');
  const[cognome,setCognome]=useState('');
  const[email,setEmail]=useState('');
  const[matricola,setMatricola]=useState('');
  const[materie,setMaterie]=useState([{label:""}]);
  //Costanti della sezione libretto modificate con set di useState
  const[appelli,setAppelli]=useState([{materia:"",voto:"",crediti:"",data:""}]);
  const[materia,setMateria]=useState('');
  const[voto,setVoto]=useState(0);
  const[crediti,setCrediti]=useState(0);
  const[credititot,setCredititot]=useState(0);
  const[media,setMedia]=useState(0);
  const[mediap,setMediap]=useState(0);
  const[data,setData]=useState(new Date());
  //Costanti della sezione avviso modificate con set di useState
  const[titolo,setTitolo]=useState('');
  const[corpoAvviso,setCorpoAvviso]=useState('');
  //Costante della sezione documenti modificate con set di useState
  const[documenti, setDocumenti] = useState();
  //Costanti della sezione test modificate con set di useState
  const[isCreaTest,setIsCreaTest]=useState(true);
  const[numeroDomande,setNumeroDomande]=useState("1");
  const[domanda,setDomanda]=useState('');
  const[opzione1,setOpzione1]=useState('');
  const[opzione2,setOpzione2]=useState('');
  const[opzione3,setOpzione3]=useState('');
  const[opzione4,setOpzione4]=useState('');
  const[rispostaCorretta,setRispostaCorretta]=useState('');
  const[i,seti]=useState(1);
  //Costanti riempite con avvisi, documenti e test personali dal database, modificate con set di useState
  const[avvisi,setAvvisi]=useState([{id:"",idavviso:"",titolo:"",corpo:"",data:"",autore:""}]);
  const[listaDocumenti,setListaDocumenti]=useState([{id:"",iddocumento:"",titolo:"",contenuto:"",autore:""}]);
  const[listaTest,setListaTest]=useState([{id:"",idtest:"",titolo:"",autore:""}]);
  //Costanti che visualizzano le varie sezioni al click o messaggi di errore
  const[isStudente,setIsStudente]=useState(false); //Nasconde le funzioni riservate ai professori
  const[isAggiungiMateria,setIsAggiungiMateria]=useState(false);
  const[show,setShow]=useState(false);
  const[erroreTitolo,setErroreTitolo]=useState(false);
  const[erroreMateria,setErroreMateria]=useState(false);
  const[erroreVoto,setErroreVoto]=useState(false);
  const[erroreCrediti,setErroreCrediti]=useState(false);
  const[msg,setMsg]=useState('');
  const[erroreEvento,setErroreEvento]=useState(false);
  const[msgAgenda,setMsgAgenda]=useState('');
  const[attenzione,setAttenzione]=useState(false);
  const[isCaricato,setIsCaricato]=useState(false);
  const[isImmagineProfilo,setIsImmagineProfilo]=useState(false);
  const[immagine,setImmagine]=useState();
  const[isGestisciEliminazioni,setIsGestisciEliminazioni]=useState(false);
  const[isGestisciEliminazionidoc,setIsGestisciEliminazionidoc]=useState(false);
  const[isGestisciEliminazionites,setIsGestisciEliminazionites]=useState(false);
  const[isGestisciEliminazioniage,setIsGestisciEliminazioniage]=useState(false);
  //Calendario
  const[schedulerData,setSchedulerData]=useState([{startDate:"",endDate:"",title:""}]);
  const[showCalendario,setShowCalendario]=useState(false);
  const[evento,setEvento]=useState("");
  const[dataEvento,setDataEvento]=useState(new Date());
  const[oraInizio,setOraInizio]=useState("08:00");
  const[oraFine,setOraFine]=useState("09:00");
  //Aggiornare dati profilo
  const[descrizione,setDescrizione]=useState("");
  const[nomeNuovo,setNomeNuovo]=useState("");
  const[cognomeNuovo,setCognomeNuovo]=useState("");
  const[matricolaNuova,setMatricolaNuova]=useState("");
  const[materieArray,setMaterieArray]=useState([{key:'', label:''}]);
  const[materieNuove,setMaterieNuove]=useState([""]);
  const[passwordNuova,setPasswordNuova]=useState("");
  const[erroreNomeNuovo,setErroreNomeNuovo]=useState(false);
  const[erroreCognomeNuovo,setErroreCognomeNuovo]=useState(false);
  const[erroreMatricolaNuova,setErroreMatricolaNuova]=useState(false);
  const[errorePasswordNuova,setErrorePasswordNuova]=useState(false);
  const[msgprofilo,setMsgProfilo]=useState("");
    
  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route

  //Quando viene caricata la pagina
  useEffect(()=>{
    //Verifica se c'è una sessione attiva
    Axios.get("http://localhost:8000/login").then((response)=>{
      if(response.data.loggedIn===true){
        setNomeutente(response.data.user[0].nomeutente);
      }
    })
    //Imposta le informazioni del profilo recuperandole dal database
    Axios.get("http://localhost:8000/recuperaprofilo").then((response)=>{
      setImmagineProfilo(Buffer.from(response.data.immagine.data, 'binary').toString('base64'));
      setNome(response.data.nome);setNomeNuovo(response.data.nome);
      setCognome(response.data.cognome);setCognomeNuovo(response.data.cognome);
      setEmail(response.data.email);
      setMatricola(response.data.matricola);setMatricolaNuova(response.data.matricola);
      setDescrizione(response.data.descrizione);
      setIsStudente(response.data.isStudente);
      if(response.data.isStudente===false){
      setMaterie([...response.data.materie]);
      for(let i=0;i<response.data.materie.length;i++){
      materieNuove[i]=response.data.materie[i].key}}
    })
    //Riempie il vettore appelli recuperandoli dal database
    Axios.get("http://localhost:8000/libretto").then((response)=>{
      setAppelli([...response.data.appelli]);
      setMedia(response.data.media);
      setMediap(response.data.mediap);
      setCredititot(response.data.crediti[0].crediti);
    })
    //Riempie il vettore dell'agenda
      Axios.get("http://localhost:8000/recuperaeventi").then((response)=>{
      if(response.data.length>0){
        setSchedulerData([...response.data]);}
      })
    //Riempie il vettore delle materie con le materie del database
    Axios.get("http://localhost:8000/recuperamaterie").then((response)=>{
      setMaterieArray([...response.data])
    })
  },[materieNuove])

  const resizeFile = (immagine) =>
  new Promise((resolve) => {
    Resizer.imageFileResizer(
      immagine,
      300,
      300,
      "JPEG",
      100,
      0,
      (uri) => {
        resolve(uri);
      },
      "file"
    );
  });

  //Modifica l'immagine del profilo salvata nel database
  const modificaImmagine=async ()=>{
    const immagineridimensionata = await resizeFile(immagine);
    const data=new FormData();
    data.append('file',immagineridimensionata);
    Axios.post('http://localhost:8000/modificaimmagine',data).then((response)=>{
        setIsImmagineProfilo(false);
        window.location.reload(); //Ricarica la pagina dopo averla cambiata
      });
  }

  //Inserisce un nuovo appello nel database
  const inserisciAppello=()=>{
    //Verifica la correttezza dei campi
    if(materia===''){
      setErroreMateria(true);
      setErroreVoto(false);
      setErroreCrediti(false);
      setMsg("Inserisci una materia");
    }else if(voto>30||(voto<18&&voto!=="0")){
      setErroreMateria(false);
      setErroreVoto(true);
      setErroreCrediti(false);
      setMsg("Inserisci un voto valido");
    }else if(crediti===0){
      setErroreMateria(false);
      setErroreVoto(false);
      setErroreCrediti(true);
      setMsg("Inserisci i crediti");
    }else{
    //Se sono tutti corretti invia i dati
    Axios.post("http://localhost:8000/inserisciappello",{
      materia:materia,
      voto:voto,
      crediti:crediti,
      data:data
    }).then((response)=>{
      if(response.data==="Appello già esistente"){
        setErroreMateria(true);
        setMsg("Appello già esistente");
      }else{
      //Resetta le variabili
      setIsAggiungiMateria(false);
      setErroreMateria(false);
      setErroreVoto(false);
      setMateria("");
      setVoto(0);
      setAppelli([...response.data.appelli]);
      setMedia(response.data.media);
      setMediap(response.data.mediap);
      setCredititot(response.data.crediti[0].crediti)}
    })}
  }

  //Rimuove gli appelli cliccando su rimuovi
  const rimuoviAppello=(elemento)=>{
    Axios.post("http://localhost:8000/rimuoviappello",{
      materia:elemento
    }).then((response)=>{
      setIsAggiungiMateria(false)
      setAppelli([...response.data.appelli]);
      setMedia(response.data.media);
      setMediap(response.data.mediap);
      setCredititot(response.data.crediti[0].crediti)})
  }

  //Pubblica un avviso
  const pubblicaAvviso=()=>{
    Axios.post('http://localhost:8000/pubblicaavviso',{
      titoloavviso:titolo,
      corpoavviso:corpoAvviso
    }).then((response)=>{
      setTitolo('');
      setCorpoAvviso('');
      setShow(true)})
  }

  //Riempie il vettore avvisi con gli avvisi personali recuperati dal database
  const recuperaAvvisiPersonali=()=>{
    Axios.get("http://localhost:8000/recuperaavvisipersonali").then((response)=>{
      setIsGestisciEliminazioni(true);
      setAvvisi([...response.data]);})
  }

  //Rimuove l'avviso dal database cliccando su rimuovi
  const rimuoviAvviso=(elemento)=>{
    Axios.post("http://localhost:8000/rimuoviavviso",{
      idavviso:elemento
    }).then((response)=>{
      setAvvisi([...response.data]);})
  }

  //Carica documenti nel database
  const upload=()=>{
    if(documenti===undefined){
    }else{
      const data=new FormData();
      data.append('file',documenti);
      Axios.post('http://localhost:8000/upload',data).then((response)=>{
          setIsCaricato(true);
          setDocumenti();
      })
    }
  }

  //Riempie il vettore listaDocumenti con gli avvisi personali recuperati dal database
  const recuperaDocumentiPersonali=()=>{
    Axios.get("http://localhost:8000/recuperadocumentipersonali").then((response)=>{
      setIsGestisciEliminazionidoc(true);
      setListaDocumenti([...response.data]);
      setDocumenti();
    })
  }

  //Rimuove il documento dal database cliccando su rimuovi
  const rimuoviDocumento=(elemento)=>{
    Axios.post("http://localhost:8000/rimuovidocumento",{
      iddocumento:elemento
    }).then((response)=>{
      setListaDocumenti([...response.data]);})
  }

  //Crea un test
  const creaTest=()=>{
    Axios.post('http://localhost:8000/createst',{
      titolotest:titolo
    }).then((response)=>{
      if(response.data==="Test già esistente"){
        setErroreTitolo(true);
      }else{
      setIsCreaTest(false);
      setErroreTitolo(false);
      }
    })
  }

  //Inserisce la domanda nel test appena creato
  const inserisciDomanda=()=>{
    Axios.post('http://localhost:8000/inseriscidomanda',{
      titolotest:titolo,
      domanda:domanda,
      opzione1:opzione1,
      opzione2:opzione2,
      opzione3:opzione3,
      opzione4:opzione4,
      rispostacorretta:rispostaCorretta
    }).then((response)=>{
      seti(i+1);
      setDomanda('');
      setOpzione1('');
      setOpzione2('');
      setOpzione3('');
      setOpzione4('');
      setRispostaCorretta('');
      //Quando sono state inserite tutte le domande ritorna la schermata per creare un nuovo test
      if(i>=numeroDomande){
        setIsCreaTest(true);
        setShow(true);
        setTitolo('');
        seti(1);
        setNumeroDomande(1);
      }
    })
  }

  //Riempie il vettore listaTest con i test personali recuperati dal database
  const recuperaTestPersonali=()=>{
    Axios.get("http://localhost:8000/recuperatestpersonali").then((response)=>{
      setIsGestisciEliminazionites(true);
      setListaTest([...response.data]);
    })
  }

  //Rimuove il test dal database cliccando su rimuovi  
  const rimuoviTest=(elemento)=>{
    Axios.post("http://localhost:8000/rimuovitest",{
      idtest:elemento
    }).then((response)=>{
      setListaTest([...response.data]);})
  }
  
  //Crea evento nell'agenda
  const creaEvento=(evento,dataevento,orainizio,orafine)=>{
    if(evento===""){
      setErroreEvento(true);
      setMsgAgenda("Inserisci un evento");
    }else{
      Axios.post("http://localhost:8000/creaevento",{
        evento:evento,
        dataevento:dataevento,
        orainizio:orainizio,
        orafine:orafine
      }).then(response=>{
        if(response.data.msg==="Evento già presente"){
          setMsgAgenda(response.data.msg);
          setErroreEvento(true);
        }else{
          setSchedulerData([...response.data]);
          setShowCalendario(true);
          setEvento("");
          setDataEvento(new Date());
          setOraInizio("08:00");
          setOraFine("09:00")
          setErroreEvento(false);
        }
      })
    }
  }

  const rimuoviEvento=(titolo,inizio,fine)=>{
    Axios.post("http://localhost:8000/rimuovievento",{
      titolo:titolo,
      oraInizio:inizio,
      oraFine:fine
    }).then((response)=>{
      setSchedulerData([...response.data]);
    })
  }

  //Aggiorna dati profilo
  const aggiornaprofilo=()=>{
    if((passwordNuova.length<8&&passwordNuova!=="")||passwordNuova.length>20){setErrorePasswordNuova(true);setMsgProfilo("Inserisci una password valida!")
    }else{
      Axios.post("http://localhost:8000/aggiornaprofilo",{
        nome:nomeNuovo,
        cognome:cognomeNuovo,
        matricola:matricolaNuova,
        materie:materieNuove,
        password:passwordNuova,
        descrizione:descrizione
      }).then((response)=>{
        if(response.data==="Inserisci un nome"){
          setErroreNomeNuovo(true);
          setErroreCognomeNuovo(false);
          setErroreMatricolaNuova(false);
          setMsgProfilo(response.data)}
        else if(response.data==="Inserisci un cognome"){
          setErroreCognomeNuovo(true);
          setErroreNomeNuovo(false);
          setErroreMatricolaNuova(false);
          setMsgProfilo(response.data)}
        else if(response.data==="Matricola già presente o non valida"){
          setErroreMatricolaNuova(true);
          setErroreCognomeNuovo(false);
          setErroreNomeNuovo(false);
          setMsgProfilo(response.data)}
        else{
          setNome(response.data.nome);setNomeNuovo(response.data.nome);
          setCognome(response.data.cognome);setCognomeNuovo(response.data.cognome);
          setMatricola(response.data.matricola);setMatricolaNuova(response.data.matricola);
          window.location.reload();
        }
      })
    }
  }

  //Esce dalla sessione e reindirizza alla pagina d'accesso
  const logout=()=>{
    Axios.post("http://localhost:8000/logout",).then((response)=>{
      history.push('/signin');
    })
  }

  //Elimina l'account e reindirizza alla pagina home
  const eliminaAccount=()=>{
    Axios.get("http://localhost:8000/eliminaaccount").then((response)=>{
      history.push('/');
    })
  }

  return (
    <div className="sfondoprofilo" onSubmit={(e)=>{e.preventDefault()}}>
      {/*Modale che viene visualizzata per eventi avvenuti con successo, quando show è true*/}
      <Modal show={show} onHide={()=>{setShow(false)}}>
        <Modal.Header closeButton>
          <Modal.Title>Fatto!</Modal.Title>
        </Modal.Header>
        <Modal.Body>Pubblicato con successo ✓</Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
      {/*Barra di navigazione*/}
      <Navbar collapseOnSelect expand="lg">
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="col d-flex justify-content-end">
            <Nav.Link href="/segui">Segui</Nav.Link>
            <Nav.Link href="/bacheca">Bacheca</Nav.Link>
            <Nav.Link href="/forum">Forum</Nav.Link>
            <Nav.Link href="/documenti">Documenti</Nav.Link>
            <Nav.Link href="/test">Test</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      {immagineProfilo===undefined&&<Spinner animation="border" variant="warning" role="status"/>}

      {immagineProfilo!==undefined&&<div className="profilo">
        <Figure className="figura">
          <Image className="immagineprofilo2" src={"data:image/jpg;charset=utf-8;base64," + immagineProfilo} alt="caricamento" roundedCircle/>
        </Figure>
        <div className="infoprofilo">
          <div className="box2">
          <div className="nomeprofilo">{nome} {cognome}</div>
          <div>📛 {nomeutente}</div>
          {/*Visualizzata solo se è uno studente, quando isStudente è true*/}
          {isStudente && <div>👨🏻‍🎓 {matricola}</div>}
          {/*Visualizzata solo se è un insegnante, quando isStudente è false*/}
          {isStudente===false && <div>📚 {materie.map((materia,index)=><span key={index}>{materia.label}<br></br></span>)}</div>}
          <div>📧 {email}</div>
          <div className="divgestisci">
                <Alert show={attenzione} variant="danger" onClose={() => setAttenzione(false)} dismissible>
                  <Alert.Heading>Attenzione!</Alert.Heading>
                  <p>Sei sicuro di voler eliminare l'account? Perderai tutti i tuoi dati in modo irreversibile.</p>
                  <Button onClick={eliminaAccount}>Procedi</Button>
                </Alert>
                {/*Visualizzato solo quando isImmagineProfilo è false*/}
                {isImmagineProfilo===false&&<Button variant="success" className="pulsanteprofilo" onClick={()=>{setIsImmagineProfilo(true)}} block>Modifica immagine del profilo</Button>}
                {/*Visualizzato solo se si clicca su Modifica immagine del profilo, quando isImmagineProfilo è true*/}
                {isImmagineProfilo&&<Form>
                  <Form.Group>
                    <Form.File type="file" name="file" onChange={(event)=>{setImmagine(event.target.files[0])}}/>
                  </Form.Group>
                  <ButtonGroup>
                    <Button variant="light" type="reset" onClick={modificaImmagine}>Conferma</Button>
                    <Button variant="light" onClick={()=>setIsImmagineProfilo(false)}>Torna indietro</Button>
                  </ButtonGroup>
                </Form>}
                {/*Visualizzato solo quando isImmagineProfilo è false*/}
                {isImmagineProfilo===false&&<Button variant="danger" className="pulsanteelimina" onClick={()=>{setAttenzione(true)}} block>Elimina Account</Button>}
              </div>
          <div className="logout" onClick={logout}>Esci</div>
          </div>
      </div>
          
        <Col className="colprofilo">
          <div className="box">
          {/*Aspetta che venga caricata l'immagine del profilo, quando immagineProfilo non è undefined*/}
          <Tabs onClick={(e)=>{if(e.target.id==="uncontrolled-tab-example-tab-calendario"){setShowCalendario(true)}}} className="gestioneprofilo" id="uncontrolled-tab-example">
            
            {/*Visualizzato solo se è uno studente, quando isStudente è true*/}
            {isStudente&&<Tab eventKey="libretto" title="Libretto">
              <Form>
                {/*Visualizzato solo quando isAggiungiMateria è false e non ci sono appelli, il vettore è vuoto*/}
                {isAggiungiMateria===false&&appelli[0]===undefined&&<Alert>Non sono presenti esami nel libretto!</Alert>}
                {/*Visualizzato solo quando isAggiungiMateria è false*/}
                {isAggiungiMateria===false&&appelli[0]!==undefined&&<Form.Group className="formlibretto">
                  <Table striped bordered hover size="sm">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Materia</th>
                        <th>Voto</th>
                        <th>Crediti</th>
                        <th>Data</th>
                      </tr>
                    </thead>
                    {appelli.map((appello,index)=><tbody key={index}>
                      <tr>
                        <td>{index+1}</td>
                        <td>{appello.materia}</td>
                        {appello.voto===0&&<td>Idoneo</td>}
                        {appello.voto!==0&&<td>{appello.voto}</td>}
                        <td>{appello.crediti}</td>
                        <td>{appello.data}</td>
                        <td><Button size="sm" variant="warning" className="pulsanteelimina" onClick={()=>{rimuoviAppello(appello.materia)}} block>Rimuovi</Button></td>
                      </tr>
                    </tbody>)}
                  </Table>
                  {media!==null&&<div>• Media voti aritmetica: {media.toFixed(3)}</div>}
                  {mediap!==null&&<div>• Media voti ponderata: {mediap.toFixed(3)}</div>}
                  {mediap!==null&&<div>• Media per conseguimento: {(mediap/3*11).toFixed(3)}</div>}
                  {credititot!==null&&<div>• Crediti totali: {credititot}</div>}
                </Form.Group>}
                {/*Visualizzato solo quando isAggiungiMateria è false*/}
                {isAggiungiMateria===false&&<Button className="pulsantelibretto" onClick={()=>{setIsAggiungiMateria(true)}}>Aggiungi esame</Button>}
                {/*Visualizzato cliccando su Aggiungi esame, quando isAggiungiMateria è true*/}
                {isAggiungiMateria&&<Form.Group>
                  <Form.Label>Materia</Form.Label>
                  <Form.Control onChange={(event)=>{setMateria(event.target.value)}} isInvalid={erroreMateria} type="text" placeholder="Inserisci materia..." />
                  <Form.Label>Voto</Form.Label>
                  <Form.Control type="number" onChange={(event)=>{setVoto(event.target.value)}} isInvalid={erroreVoto} placeholder="Inserisci voto..." />
                  <Form.Text className="text-muted">Deve essere compreso tra 18-30. Inserire 0 per indicare l'idoneità</Form.Text>
                  <Form.Label>Crediti</Form.Label>
                  <Form.Control type="number" onChange={(event)=>{setCrediti(event.target.value)}} isInvalid={erroreCrediti} placeholder="Inserisci crediti..." />
                  <Form.Label>Data</Form.Label><br></br>
                  <DatePicker selected={data} onChange={date => setData(date)}/>
                  <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
                </Form.Group>}
                {/*Visualizzato cliccando su Aggiungi esame, quando isAggiungiMateria è true*/}
                {isAggiungiMateria&&<Form.Group>
                  <ButtonGroup>
                    <Button type="submit" onClick={inserisciAppello}>Inserisci</Button>
                    <Button onClick={()=>setIsAggiungiMateria(false)}>Torna indietro</Button>
                  </ButtonGroup>
                </Form.Group>}
              </Form>
            </Tab>}
            {/*Visualizzato solo se è un insegnante, quando isStudente è false*/}
            {isStudente===false &&<Tab eventKey="avviso" title="Pubblica un avviso">
              {isGestisciEliminazioni===false&&<Form>
                <Form.Label>Titolo</Form.Label>
                <Form.Control onChange={(event)=>{setTitolo(event.target.value)}} type="text" value={titolo} placeholder="Inserisci titolo..." /> 
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Corpo</Form.Label>
                  <Form.Control className="areatesto" onChange={(event)=>{setCorpoAvviso(event.target.value)}} value={corpoAvviso} placeholder="Inserisci corpo..." as="textarea" rows={10} />
                </Form.Group>
                <ButtonGroup>
                  <Button type="reset" onClick={pubblicaAvviso} variant="primary">Pubblica</Button>
                  <Button type="reset" onClick={recuperaAvvisiPersonali} variant="primary">Gestisci i tuoi avvisi</Button>
                </ButtonGroup>
              </Form>}
              {/*Visualizzato cliccando su Gesistisci i tuoi avvisi, quando isGestisciEliminazioni è true e non ci sono avvisi, vettore vuoto*/}
              {isGestisciEliminazioni&&avvisi[0]===undefined&&<Alert>Non hai pubblicato nessun avviso!</Alert>}
              {/*Visualizzato cliccando su Gestisci i tuoi avvisi, quando isGestisciEliminazioni è true e ci sono avvisi, vettore non vuoto*/}
              {isGestisciEliminazioni&&avvisi[0]!==undefined&&<Table striped bordered hover size="sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Titolo</th>
                  </tr>
                </thead>
                {avvisi.map((avviso,index)=><tbody key={index}>
                  <tr>
                    <td>{index+1}</td>
                    <td>{avviso.titolo}</td>
                    <td><Button size="sm" variant="outline-danger" onClick={()=>{rimuoviAvviso(avviso.idavviso)}} block>Rimuovi</Button></td>
                  </tr>
                </tbody>)}
              </Table>}
              {/*Visualizzato cliccando su Gestisci i tuoi avvisi*/}
              {isGestisciEliminazioni&&<Button onClick={()=>setIsGestisciEliminazioni(false)}>Torna indietro</Button>}
            </Tab>}
            <Tab eventKey="documento" title="Carica un documento">
              {/*Visualizzato solo se isGestisciEliminazionidoc è false*/}
              {isGestisciEliminazionidoc===false&&<Form className="formdocumenti">
                <Form.Group> 
                  <Form.File type="file" name="file" onChange={(event)=>{setDocumenti(event.target.files[0])}}/>
                </Form.Group>
                <ButtonGroup>
                  <Button type="reset" onClick={upload}>Carica</Button>
                  <Button className="pulsantegestisci" onClick={recuperaDocumentiPersonali} variant="primary">Gestisci i tuoi documenti</Button>
                </ButtonGroup>
              </Form>}
              {/*Visualizzato solo cliccando su Gestisci i tuoi documenti, quando isGestisciEliminazionidoc è true e non ci sono documenti, vettore vuoto*/}
              {isGestisciEliminazionidoc&&listaDocumenti[0]===undefined&&<Alert>Non hai caricato nessun documento!</Alert>}
              {/*Visualizzato solo cliccando su Gestisci i tuoi documenti, quando isGestisciEliminazionidoc è true e ci sono documenti, vettore non vuoto*/}
              {isGestisciEliminazionidoc&&listaDocumenti[0]!==undefined&&<Table striped bordered hover size="sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Titolo</th>
                  </tr>
                </thead>
                {listaDocumenti.map((documento,index)=><tbody key={index}>
                  <tr>
                    <td>{index+1}</td>
                    <td>{documento.titolo}</td>
                    <td><Button size="sm" variant="outline-danger" onClick={()=>{rimuoviDocumento(documento.iddocumento)}} block>Rimuovi</Button></td>
                  </tr>
                </tbody>)}
              </Table>}
              {/*Visualizzato solo cliccando su Gestisci i tuoi documenti, quando isGestisciEliminazionidoc è true*/}
              {isGestisciEliminazionidoc&&<Button onClick={()=>setIsGestisciEliminazionidoc(false)}>Torna indietro</Button>}
              {/*Modale visualizzata soltanto se il documento viene caricato con successo, quando isCaricato è true*/}
              <Modal show={isCaricato} onHide={()=>{setIsCaricato(false)}}>
                <Modal.Header closeButton>
                  <Modal.Title>Fatto!</Modal.Title>
                </Modal.Header>
                <Modal.Body>Caricato con successo ✓</Modal.Body>
                <Modal.Footer></Modal.Footer>
              </Modal>
            </Tab>
            {/*Visualizzato solo se è un insegnante, quando isStudente è false*/}
            {isStudente===false &&<Tab eventKey="test" title="Crea un test">
              {/*Visualizzato solo quando isCreatest è true e isGestisciEliminazionites è false*/}
              {isCreaTest&&isGestisciEliminazionites===false &&<Form>
                <Form.Group>
                  <Form.Label>Titolo</Form.Label>
                  <Form.Control className="areatesto" onChange={(event)=>{setTitolo(event.target.value)}} isInvalid={erroreTitolo} type="text" value={titolo} placeholder="Inserisci titolo..." />
                  <Form.Control.Feedback type="invalid">Test già esistente, inserisci un titolo diverso</Form.Control.Feedback>
                  <Form.Label>Seleziona il numero di domande</Form.Label>
                  <Form.Control type="number" defaultValue="1" onChange={(event)=>{setNumeroDomande(event.target.value)}}></Form.Control>
                </Form.Group>
                <ButtonGroup>
                  <Button type="submit" onClick={creaTest} variant="primary">Crea</Button>
                  <Button type="reset" onClick={recuperaTestPersonali} variant="primary">Gestisci i tuoi test</Button>
                </ButtonGroup>
              </Form>}
              {/*Visualizzato cliccando su Crea, quando isCreaTest è false*/}
              {isCreaTest===false&&isGestisciEliminazionites===false&&<Form>
                <Form.Group>
                  <Form.Label>Inserisci la domanda n° {i}</Form.Label>
                  <Form.Control onChange={(event)=>{setDomanda(event.target.value)}} type="text" value={domanda} placeholder="Inserisci domanda..." />
                </Form.Group>
                <Form.Group>
                  <Form.Row id="form" className="opzioni">
                    <Form.Group as={Col} onChange={(event)=>{setOpzione1(event.target.value)}}>
                      <Form.Label>Opzione 1:</Form.Label>
                      <Form.Control type="text" value={opzione1} placeholder="Inserisci la prima opzione..."/>
                    </Form.Group>
                    <Form.Group as={Col} onChange={(event)=>{setOpzione2(event.target.value)}}>
                      <Form.Label>Opzione 2:</Form.Label>
                      <Form.Control type="text" value={opzione2} placeholder="Inserisci la seconda opzione..."/>
                    </Form.Group>
                    <Form.Group as={Col} onChange={(event)=>{setOpzione3(event.target.value)}}>
                      <Form.Label>Opzione 3:</Form.Label>
                      <Form.Control type="text" value={opzione3} placeholder="Inserisci la terza opzione..."/>
                    </Form.Group>
                    <Form.Group as={Col} onChange={(event)=>{setOpzione4(event.target.value)}}>
                      <Form.Label>Opzione 4:</Form.Label>
                      <Form.Control type="text" value={opzione4} placeholder="Inserisci la quarta opzione..."/>
                    </Form.Group>
                  </Form.Row>
                  <Form.Group onChange={(event)=>{setRispostaCorretta(event.target.value)}}>
                    <Form.Label>Risposta corretta:</Form.Label>
                    <Row>
                    <Form.Check className="risposte"
                      type="radio"
                      label="opzione 1"
                      name="formHorizontalRadios"
                      id="1"
                      value={opzione1}
                    />
                    <Form.Check className="risposta"
                      type="radio"
                      label="opzione 2"
                      name="formHorizontalRadios"
                      id="2"
                      value={opzione2}
                    />
                    <Form.Check className="risposta"
                      type="radio"
                      label="opzione 3"
                      name="formHorizontalRadios"
                      id="3"
                      value={opzione3}
                    />
                    <Form.Check className="risposta"
                      type="radio"
                      label="opzione 4"
                      name="formHorizontalRadios"
                      id="4"
                      value={opzione4}
                    />
                    </Row>
                  </Form.Group>
                  <Button type="reset" onClick={inserisciDomanda} variant="primary">Avanti</Button>
                </Form.Group>
              </Form>}
              {/*Visualizzato solo cliccando su Gestisci i tuoi test, quando isGestisciEliminazionites è true e ci sono test, vettore non vuoto*/}{isGestisciEliminazionites&&listaTest[0]===undefined&&<Alert>Non hai creato nessun test!</Alert>}
              {isGestisciEliminazionites&&listaTest[0]!==undefined&&<Table striped bordered hover size="sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Titolo</th>
                  </tr>
                </thead>
                {listaTest.map((test,index)=><tbody key={index}>
                  <tr>
                    <td>{index+1}</td>
                    <td>{test.titolo}</td>
                    <td><Button size="sm" variant="outline-danger" onClick={()=>{rimuoviTest(test.idtest)}} block>Rimuovi</Button></td>
                  </tr>
                </tbody>)}
              </Table>}
              {/*Visualizzato solo cliccando su Gestisci i tuoi documenti, quando isGestisciEliminazionidoc è true*/}
              {isGestisciEliminazionites&&<Button onClick={()=>setIsGestisciEliminazionites(false)}>Torna indietro</Button>}
            </Tab>}
            <Tab eventKey="calendario" title="Calendario">
              <Paper>
                {showCalendario&&<Scheduler locale="it" data={schedulerData}>
                  <ViewState defaultCurrentDate={moment().format("YYYY-MM-DD")}/>
                  <WeekView startDayHour={8} endDayHour={20}/>
                  <Toolbar/>
                  <DateNavigator />
                  <Appointments/><AppointmentTooltip/>
                </Scheduler>}
                {showCalendario&&<ButtonGroup className="buttoncalendario">
                  <Button onClick={()=>setShowCalendario(false)}>Aggiungi evento</Button>
                  <Button onClick={()=>{setIsGestisciEliminazioniage(true);setShowCalendario(false)}}>Rimuovi evento</Button>
                </ButtonGroup>}
                {showCalendario===false&&isGestisciEliminazioniage===false&&<Form className="formcalendario">
                  <Form.Label>Evento</Form.Label>
                  <Form.Control type="text" onChange={(e)=>setEvento(e.target.value)} required isInvalid={erroreEvento}></Form.Control>
                  <Form.Label>Data</Form.Label>
                  <DatePicker selected={dataEvento} onChange={date => setDataEvento(date)}/><br></br>
                  <Form.Label>Ora inizio</Form.Label>
                  <TimePicker value={oraInizio} disableClock={true} clearIcon={null} onChange={time=>setOraInizio(time)}></TimePicker><br></br>
                  <Form.Label>Ora fine</Form.Label>
                  <TimePicker value={oraFine} disableClock={true} clearIcon={null} onChange={time=>setOraFine(time)}></TimePicker>
                  <Form.Control.Feedback type="invalid">{msgAgenda}</Form.Control.Feedback>
                </Form>}
                {showCalendario===false&&isGestisciEliminazioniage===false&&<ButtonGroup className="buttoncalendario">
                  <Button onClick={()=>{creaEvento(evento,dataEvento,oraInizio,oraFine)}}>Conferma</Button>
                  <Button onClick={()=>{setShowCalendario(true);setErroreEvento(false);setEvento("");setDataEvento(new Date());setOraInizio("08:00");setOraFine("09:00")}}>Torna indietro</Button>
                </ButtonGroup>}
                {isGestisciEliminazioniage&&(schedulerData[0]===undefined||schedulerData[0].title==="")&&<Alert>Non hai nessun evento in programma!</Alert>}
                {isGestisciEliminazioniage&&schedulerData[0]!==undefined&&schedulerData[0].title!==""&&<Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Titolo</th>
                      <th>Data</th>
                      <th>Ora inizio</th>
                      <th>Ora fine</th>
                    </tr>
                  </thead>
                  {schedulerData.map((evento,index)=><tbody key={index}>
                    <tr>
                      <td>{index+1}</td>
                      <td>{evento.title}</td>
                      <td>{evento.startDate.substr(0,10)}</td>
                      <td>{evento.startDate.substr(11)}</td>
                      <td>{evento.endDate.substr(11)}</td>
                      <td><Button size="sm" variant="outline-danger" onClick={()=>{rimuoviEvento(evento.title,evento.startDate,evento.endDate)}} block>Rimuovi</Button></td>
                    </tr>
                  </tbody>)}
                </Table>}
                {/*Visualizzato solo cliccando su Gestisci i tuoi documenti, quando isGestisciEliminazionidoc è true*/}
                {isGestisciEliminazioniage&&<Button onClick={()=>{setIsGestisciEliminazioniage(false);setShowCalendario(true)}}>Torna indietro</Button>}
              </Paper>
            </Tab>
            <Tab eventKey="aggiorna" title="Aggiorna dati profilo">
                <Form>
                  <Form.Label>Nome</Form.Label>
                  <Form.Control defaultValue={nome} isInvalid={erroreNomeNuovo} placeholder={nome} onChange={(e)=>{setNomeNuovo(e.target.value)}}/>
                  <Form.Label>Cognome</Form.Label>
                  <Form.Control defaultValue={cognome} isInvalid={erroreCognomeNuovo} placeholder={cognome} onChange={(e)=>{setCognomeNuovo(e.target.value)}}/>
                  {isStudente&&<div><Form.Label>Matricola</Form.Label>
                  <Form.Control defaultValue={matricola} isInvalid={erroreMatricolaNuova} placeholder={matricola} onChange={(e)=>{setMatricolaNuova(e.target.value)}}/></div>}
                  {isStudente===false&&<div><Form.Label>Seleziona le materie</Form.Label>
                  <DropdownMultiselect placeholderMultipleChecked="Più materie selezionate" placeholder="Nessuna materia selezionata" handleOnChange={(selected)=>{setMaterieNuove(selected)}} options={materieArray} name="countries"/></div>}
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" isInvalid={errorePasswordNuova} onChange={(e)=>{setPasswordNuova(e.target.value)}}/>
                  <Form.Text className="text-muted">Deve essere compresa tra 8-20 caratteri.<br></br>Non variando i campi non vengono modificati i dati.</Form.Text>
                  <Form.Label>Descrizione</Form.Label>
                  <Form.Control className="areatesto" defaultValue={descrizione} placeholder="Inserisci descrizione..." as="textarea" rows={10} onChange={(e)=>{setDescrizione(e.target.value)}}/>
                  <Form.Control.Feedback type="invalid">{msgprofilo}</Form.Control.Feedback>
                  <Button className="pulsanteregistrazione" onClick={aggiornaprofilo}>Salva modifiche</Button>
                </Form>
            </Tab>
          </Tabs>
          </div>
        </Col>
      </div>}
    </div>
  )
}

export default withRouter(Profilo)