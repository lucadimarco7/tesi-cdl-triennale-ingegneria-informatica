import React,{useState,useEffect} from "react"; //React-App
import {withRouter} from 'react-router-dom'; //Per gestire le routes
import {createBrowserHistory as createHistory} from 'history'; //Per i reindirizzamenti
import {Form,Button,Col,Nav,InputGroup,Modal,Image} from 'react-bootstrap'; //Elementi di Bootstrap
import DropdownMultiselect from 'react-multiselect-dropdown-bootstrap'; //Menù a scelta multipla di Bootstrap
import Axios from 'axios'; //Per le richieste tra Client e Server
import image from '../images/logo3.png'; //Logo del sito

function Registrazione() {
  //Costanti del form di registrazione modificate con set di useState 
  const[nomeReg,setNome]=useState('');
  const[cognomeReg,setCognome]=useState('');
  const[nomeutenteReg,setNomeutenteReg]=useState('');
  const[emailReg,setEmail]=useState('');
  const[passwordReg,setPasswordReg]=useState('');
  const[professioneReg,setProfessioneReg]=useState('1');
  const[matricolaReg,setMatricolaReg]=useState('');
  const[materieReg,setMaterieReg]=useState([]);
  //Costanti di messaggi di errore in caso di compilazione errata del form
  const[msg,setMsg]=useState('');
  const[show,setShow]=useState(false);
  const[showmat,setShowMat]=useState(true);
  const[erroreNome,setErroreNome]=useState(false);
  const[erroreCognome,setErroreCognome]=useState(false);
  const[erroreNomeUtente,setErroreNomeUtente]=useState(false);
  const[erroreMail,setErroreMail]=useState(false);
  const[errorePassword,setErrorePassword]=useState(false);
  const[erroreMatricola,setErroreMatricola]=useState(false);
  //Vettore delle materie da visualizzare nel menù a scelta multipla per insegnanti
  const[materieArray,setMaterieArray]=useState([{key:'', label:''}]);

  const history=createHistory({forceRefresh:true}); //Carica la nuova pagina al cambio di route

  Axios.defaults.withCredentials=true; //Serve per leggere la sessione e i cookie prima delle richieste Axios

  //Quando viene caricata la pagina
  useEffect(()=>{
    //Verifica se c'è una sessione attiva
    Axios.get("http://localhost:8000/login").then((response)=>{
      if(response.data.loggedIn===true){
        history.push('/bacheca')
      }
    })
    //Riempie il vettore delle materie con le materie del database
    Axios.get("http://localhost:8000/recuperamaterie").then((response)=>{
      setMaterieArray([...response.data])
  })},[history])

  //Registra l'utente, inviando i dati del form e ritornado un messaggio di successo o gli eventuali errori
  const registra=()=>{
    //Verifica la correttezza dei campi e mostra eventuali messaggi di errore
    if(nomeReg===''){
      setMsg("Inserisci un nome");
      setErroreNome(true);
      setErroreCognome(false);
      setErroreNomeUtente(false);
      setErroreMail(false);
      setErrorePassword(false);
      setErroreMatricola(false);
    }else if(cognomeReg===''){
      setMsg("Inserisci un cognome");
      setErroreNome(false);
      setErroreCognome(true);
      setErroreNomeUtente(false);
      setErroreMail(false);
      setErrorePassword(false);
      setErroreMatricola(false);
    }else if(nomeutenteReg===''){
      setMsg("Inserisci un nome utente");
      setErroreNome(false);
      setErroreCognome(false);
      setErroreNomeUtente(true);
      setErroreMail(false);
      setErrorePassword(false);
      setErroreMatricola(false);
    }else if(emailReg===''){
      setMsg("Inserisci un`e-mail");
      setErroreNome(false);
      setErroreCognome(false);
      setErroreNomeUtente(false);
      setErroreMail(true);
      setErrorePassword(false);
      setErroreMatricola(false);
    }else if(passwordReg.length<8 || passwordReg.length>20){
      setMsg("Inserisci una password corretta");
      setErroreNome(false);
      setErroreCognome(false);
      setErroreNomeUtente(false);
      setErroreMail(false);
      setErrorePassword(true);
      setErroreMatricola(false);
    }else if(professioneReg==='1'&&matricolaReg===''){
    setMsg("Inserisci una matricola");
    setErroreNome(false);
    setErroreCognome(false);
    setErroreNomeUtente(false);
    setErroreMail(false);
    setErrorePassword(false);
    setErroreMatricola(true);
    }else{
      Axios.post("http://localhost:8000/registra",{
        nome:nomeReg,
        cognome:cognomeReg,
        nomeutente:nomeutenteReg,
        email:emailReg,
        password:passwordReg,
        professione:professioneReg,
        matricola:matricolaReg,
        materie:materieReg
      }).then((response)=>{
        if(response.data.reg===false){
          setMsg(response.data.msg);
          if(response.data.msg==='Nome utente non disponibile'){
            setErroreNome(false);
            setErroreCognome(false);
            setErroreNomeUtente(true);
            setErroreMail(false);
            setErrorePassword(false);
            setErroreMatricola(false);
          }else if(response.data.msg==='E-mail già presente'){
            setErroreNome(false);
            setErroreCognome(false);
            setErroreNomeUtente(false);
            setErroreMail(true);
            setErrorePassword(false);
            setErroreMatricola(false);
          }else if(response.data.msg==='Matricola già presente'||response.data.msg==='Inserisci una matricola'){
            setErroreNome(false);
            setErroreCognome(false);
            setErroreNomeUtente(false);
            setErroreMail(false);
            setErrorePassword(false);
            setErroreMatricola(true);
          }
        }else{
          setShow(true);
          setErroreNome(false);
          setErroreCognome(false);
          setErroreNomeUtente(false);
          setErroreMail(false);
          setErrorePassword(false);
          setErroreMatricola(false);
        }  
      })
    }
  }

  return (
    <div className="sfondoregistrazione">
      <div className="containerregistrazione">
        
            <Nav className="navregistrazione">
              <Nav.Item>
                  <Nav.Link href="/">Home</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                  <Nav.Link href="/signin">Accedi</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                  <Nav.Link eventKey="disabled" disabled>Registrati</Nav.Link>
              </Nav.Item>
              <div className="col d-flex justify-content-end">
                <Image className="immaginehome" src={image}/>
              </div>
            </Nav>
            {/*Modale che viene visualizzata solo in caso di successo quando show diventa true*/}
            <Modal show={show} onHide={()=>{setShow(false);window.location.reload()}}>
              <Modal.Header closeButton>
                <Modal.Title>Fatto!</Modal.Title>
              </Modal.Header>
              <Modal.Body>Registrazione avvenuta con successo ✓</Modal.Body>
              <Modal.Footer>
                <Button variant="primary" onClick={()=>{history.push('/signin')}}>Accedi</Button>
                <Button variant="danger" onClick={()=>{setShow(false);window.location.reload()}}>Chiudi</Button>
              </Modal.Footer>
            </Modal>
            <Form className="formregistrazione" onSubmit={(e)=>{e.preventDefault()}}>
              <Form.Group controlId="formGridName" onChange={(event)=>{setNome(event.target.value)}}>
                <Form.Label>Nome</Form.Label>
                <Form.Control required isInvalid={erroreNome} placeholder="Inserisci nome..." />
                <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="formGridAddress2" onChange={(event)=>{setCognome(event.target.value)}}>
                <Form.Label>Cognome</Form.Label>
                <Form.Control required isInvalid={erroreCognome} placeholder="Inserisci cognome..." />
                <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
              </Form.Group>
              <Form.Group>
                <Form.Label>Nome utente</Form.Label>
                <InputGroup 
                    onChange={(event)=>{setNomeutenteReg(event.target.value);}} hasValidation>
                  <InputGroup.Prepend>
                    <InputGroup.Text>@</InputGroup.Text>
                  </InputGroup.Prepend>     
                  <Form.Control type="text" required isInvalid={erroreNomeUtente} placeholder="Inserisci nome utente..."/>
                  <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
                </InputGroup>
              </Form.Group>
              <Form.Row id="form" className="registration">
                <Form.Group as={Col} controlId="formGridEmail" onChange={(event)=>{setEmail(event.target.value)}}>
                  <Form.Label>E-mail</Form.Label>
                  <Form.Control type="email" isInvalid={erroreMail} placeholder="Inserisci e-mail..."/>
                  <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" isInvalid={errorePassword} placeholder="Inserisci password..." onChange={(event)=>{setPasswordReg(event.target.value);}}/>
                  <Form.Text className="text-muted">Deve essere compresa tra 8-20 caratteri.</Form.Text>
                  <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
                </Form.Group> 
              </Form.Row>
              <Form.Row id="form" className="registration">
                <Form.Group as={Col} controlId="exampleForm.ControlSelect1">
                  <Form.Label>Seleziona la tua professione</Form.Label>
                  <Form.Control as="select" defaultValue="1" onChange={(event)=>{setProfessioneReg(event.target.value);if(event.target.value==='1'){setShowMat(true)}else{setShowMat(false)}}}>
                    <option value='1'>Studente/Studentessa</option>
                    <option value='2'>Professore/Professoressa</option>
                  </Form.Control>
                </Form.Group>
                {/*Se si seleziona studente showmat è true e viene visualizzato il campo matricola*/}
                {showmat &&<Form.Group as={Col} controlId="formGridMatricola" onChange={(event)=>{setMatricolaReg(event.target.value)}}>
                  <Form.Label>Matricola</Form.Label>
                  <Form.Control type="text" isInvalid={erroreMatricola} placeholder="Inserisci matricola..."/>
                  <Form.Control.Feedback type="invalid">{msg}</Form.Control.Feedback>
                </Form.Group>}
                {/*Se si seleziona insegnante showmat è false e viene visualizzato il campo materie*/}
                {showmat===false &&<Form.Group as={Col} controlId="formGridMaterie" data-width={false}>
                  <Form.Label>Seleziona le materie</Form.Label>
                  <DropdownMultiselect placeholderMultipleChecked="Più materie selezionate" placeholder="Nessuna materia selezionata" selectDeselectLabel="Seleziona/Deseleziona tutto" handleOnChange={(selected)=>{setMaterieReg(selected)}} options={materieArray} name="countries"/>
                </Form.Group>}
              </Form.Row>
              <Form.Group>
                <Button variant="primary" type="submit" className="pulsanteregistrazione" onClick={registra}>Conferma</Button>
              </Form.Group>
            </Form>
      </div>
    </div>
  )
}

export default withRouter(Registrazione)