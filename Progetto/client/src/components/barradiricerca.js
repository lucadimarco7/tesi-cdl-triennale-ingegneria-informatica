const Search = () => {
    return <form action="" method="get">
        <input className="barradiricerca" type="text" name="s" placeholder="Filtra i risultati..."/>
        <button type="submit" style={{display: 'none'}}>🔍</button>
    </form>
}

export default Search;