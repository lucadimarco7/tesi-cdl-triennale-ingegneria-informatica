import React from 'react';
import { useState } from 'react';

const Impaginazione=({vettore:Vettore})=>{
const[paginacorrente,setPaginacorrente]=useState(1);
const[elementipagina,setElementipagina]=useState(4);
const handleClick=(event)=>{
  setPaginacorrente(event.target.id)
}
const indiceUltimoElemento = paginacorrente * elementipagina;
const indicePrimoElemento = indiceUltimoElemento - elementipagina;
const elementicorrenti = vettore.slice(indicePrimoElemento, indiceUltimoElemento);

const renderElementi = elementicorrenti.map((item, index) => {
  return <li key={index}>{item.label}</li>;
});

const numeriPagina = [];
    for (let i = 1; i <= Math.ceil(vettore.length / elementipagina); i++) {
      numeriPagina.push(i);
    }

    const renderNumeriPagina = numeriPagina.map(numero => {
      return (
        <li
          key={numero}
          id={numero}
          onClick={handleClick}
        >
          {numero}
        </li>
      );
    });
    return (
        <div>
          <ul>
            {renderElementi}
          </ul>
          <ul id="page-numbers">
            {renderNumeriPagina}
          </ul>
        </div>
      );
}

export default Impaginazione