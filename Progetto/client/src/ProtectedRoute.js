import React from 'react'; //React-App
import {Route, Redirect} from 'react-router-dom'; //Per gestire le routes

function ProtectedRoute({isauth:isAuth, component:Component, ...rest}){
    return( 
        <Route 
            {...rest}
            render={(props)=>{
                /*Se è autenticato ritorna la pagina, altrimenti reindirizza alla home*/
                if(isAuth){
                    return <Component />
                }else{
                    return (
                        <Redirect to={{pathname: '/', state:{from:props.location}}}/>
                    )
                }
            }}
        />
    )
}

export default ProtectedRoute