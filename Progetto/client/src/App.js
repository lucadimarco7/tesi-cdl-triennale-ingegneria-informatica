import React,{useEffect,useState} from "react"; //React-App
import Axios from 'axios'; //Per le richieste tra Client e Server
import {BrowserRouter as Router, Switch} from "react-router-dom"; //Per gestire le routes
import ProtectedRoute from './ProtectedRoute'; //Importa la route protetta
//Importa tutte le pagine del sito
import Home from './pages/Home';
import Registrazione from './pages/Registrazione';
import Accesso from './pages/Accesso';
import Segui from "./pages/Segui";
import Bacheca from "./pages/Bacheca";
import Forum from "./pages/Forum";
import Documenti from './pages/Documenti';
import Test from "./pages/Test";
import Profilo from './pages/Profilo';

import 'bootstrap/dist/css/bootstrap.min.css'; //Libreria Bootstrap
import './App.css'; //css personale

function App() {
  const[isAuth, setIsAuth]=useState(true);
  Axios.defaults.withCredentials=true;

  //Quando viene caricata la pagina
  useEffect(()=>{ 
    //Imposta la sessione
    Axios.get("http://localhost:8000/login").then((response)=>{
      setIsAuth(response.data.loggedIn);
    })
  },[])

  return (
    <Router>
      <Switch>
        <Home exact path='/'/>
        <ProtectedRoute exact path='/segui' component={Segui} isauth={isAuth}/>
        <ProtectedRoute exact path='/bacheca' component={Bacheca} isauth={isAuth}/>
        <ProtectedRoute exact path='/forum' component={Forum} isauth={isAuth}/>
        <ProtectedRoute exact path='/profilo' component={Profilo} isauth={isAuth}/>
        <ProtectedRoute exact path='/documenti' component={Documenti} isauth={isAuth}/>
        <ProtectedRoute exact path='/test' component={Test} isauth={isAuth}/>
        <Registrazione exact path='/signup'/>
        <Accesso exact path='/signin'/>
      </Switch>
    </Router>
  );
}

export default App;
